import { SLC_LIST_PENDING, SLC_LIST_SUCCESS, SLC_LIST_ERROR } from '../action/action.types'

const initialState = {
  loading: true,
  data: '',
  error: ''
};

export default (state = initialState, action) => {

  switch (action.type) {

    case SLC_LIST_PENDING:
      return {
        ...state,
        loading: true,
      };

    case SLC_LIST_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case SLC_LIST_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};