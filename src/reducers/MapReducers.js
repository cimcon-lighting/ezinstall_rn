
import {
  SLC_LIST_ON_MAP_SUCCESS,
  SLC_LIST_ON_MAP_ERROR,
} from "../action/action.types";

const initialState = {
  loading: true,
  data: '',
  error: ''
};

export default (state = initialState, action) => {

  switch (action.type) {

    case SLC_LIST_ON_MAP_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case SLC_LIST_ON_MAP_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};
