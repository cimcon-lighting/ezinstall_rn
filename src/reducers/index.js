import { combineReducers } from 'redux'

import ApiReducers from './ApiReducers'
import SLCListReducers from './SLCListReducers'
import SearchViewReducers from './SearchViewReducers'
import SecurityCodeReducers from './SecurityCodeReducers'
import LoginReducers from './LoginReducers'
import BiometricReducers from './BiometricReducers'
import NavigationReducers from './NavigationReducers'
import SLCDetailsReducers from './SLCDetailsReducers'
import DetailsMapViewReducers from './DetailsMapViewReducers'
import AddMacReducers from './AddMacReducers'
import MapReducers from './MapReducers'
import AddSLCReducers from './AddSLCReducers'
import SettingReducers from './SettingReducers'
import LanguageReducers from './LanguageReducers'

export default combineReducers({
    ApiReducers,
    LoginReducers,
    SearchViewReducers,
    SLCListReducers,
    SecurityCodeReducers,
    BiometricReducers,
    NavigationReducers,
    SLCDetailsReducers,
    DetailsMapViewReducers,
    AddMacReducers,
    MapReducers,
    AddSLCReducers,
    SettingReducers,
    LanguageReducers,
})