import {
  LOGIN_PENDING,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
} from '../action/action.types'

const initialState = {
  loading: true,
  data: '',
  error: '',
};

export default (state = initialState, action) => {

  switch (action.type) {

    case LOGIN_PENDING:
      return {
        ...state,
        loading: true,
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};
