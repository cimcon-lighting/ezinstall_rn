
import { BIOMETRIC_SUPPORTED, BIOMETRIC_SUCCESS, BIOMETRIC_FAIED } from '../action/action.types'

const initialState = {
    data: '',
    supported: ''
};

export default (state = initialState, action) => {

    switch (action.type) {

        case BIOMETRIC_SUPPORTED:
            return {
                ...state,
                supported: action.payload,
            };

        case BIOMETRIC_SUCCESS:
            return {
                ...state,
                data: action.payload,
            };

        case BIOMETRIC_FAIED:
            return {
                ...state,
                data: action.payload
            };

        default:
            return state;
    }
};
