import { MOVE_TO_ERROR_MAC, MOVE_TO_SCREEN_FROM_MAC_SCAN, IS_LOADING, IS_CONTINUE_DIALOG } from '../action/action.types'

const initialState = {
    screen: '',
    error: '',
    data: '',
};

export default (state = initialState, action) => {

    switch (action.type) {

        case IS_LOADING:
            return {
                ...state,
                screen: '',
                error: '',
                data: '',
            };

        case IS_CONTINUE_DIALOG:

            return {
                ...state,
                data: action.payload,
                screen: '',
                error: '',
            };

        case MOVE_TO_SCREEN_FROM_MAC_SCAN:
            return {
                ...state,
                screen: action.payload,
                error: '',
                data: action.data,
            };

        case MOVE_TO_ERROR_MAC:
            return {
                ...state,
                screen: '',
                error: action.payload,
                data: '',
            };

        default:
            return state;
    }
};
