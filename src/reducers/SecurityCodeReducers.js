
import { SCODE_PENDING, SCODE_SUCCESS, SCODE_ERROR } from '../action/action.types'

const initialState = {
  loading: true,
  data: '',
  error: ''
};

export default (state = initialState, action) => {

  switch (action.type) {

    case SCODE_PENDING:
      return {
        ...state,
        loading: true,
      };

    case SCODE_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case SCODE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};
