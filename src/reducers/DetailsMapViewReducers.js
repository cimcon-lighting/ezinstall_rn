import {MAP_SHOW} from '../action/action.types'

const initialState = {
    isMapViewOpen : false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case MAP_SHOW:
            return {
                ...state,
                isMapViewOpen : action.payload
            }
        default:
            return state;
    }
}