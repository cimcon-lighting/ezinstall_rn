import { LANGUAGE_SUCCESS,LANGUAGE_ERROR } from '../action/action.types'

const initialState = {
  loading: true,
  data: '',
  error: ''
};

export default (state = initialState, action) => {

  switch (action.type) {
      case LANGUAGE_SUCCESS:
        return {
          ...state,
          data: action.payload,
          loading: false
        };
  
      case LANGUAGE_ERROR:
        return {
          ...state,
          loading: false,
          error: action.payload
        };

    default:
      return state;
  }
};