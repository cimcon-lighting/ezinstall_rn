import {
    LOGIN_SUCCESS_NAV,
    SECURITY_SUCCESS_NAV,
    LOGOUT_NAV
} from '../action/action.types'

const initialState = {
    isLoginSuccess: false,
    isSecuritySuccess: false,
    isLoading: true,
};

export default (state = initialState, action) => {

    switch (action.type) {

        case LOGIN_SUCCESS_NAV:
            return {
                ...state,
                isLoginSuccess: action.payload,
                isLoading: false,

            };

        case SECURITY_SUCCESS_NAV:
            return {
                ...state,
                isSecuritySuccess: action.payload,
                isLoading: false,
            };

            case LOGOUT_NAV:
                console.log("LOGOUT REDUCER:")
                return{
                    ...state,
                    isSecuritySuccess:false,
                    isLoginSuccess:false,
                    isLoading: false
                }

        default:
            return state;
    }
};
