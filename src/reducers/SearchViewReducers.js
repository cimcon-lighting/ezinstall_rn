import {SEARCH_SHOW} from '../action/action.types'

const initialState = {
    isSearchOpen : false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SEARCH_SHOW:
            return {
                ...state,
                isSearchOpen : action.payload
            }
        default:
            return state;
    }
}