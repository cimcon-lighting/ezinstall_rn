
import { API_PENDING, API_SUCCESS, API_ERROR } from '../action/action.types'

const initialState = {
  loading: true,
  data: '',
  error: ''
};

export default (state = initialState, action) => {

  switch (action.type) {

    case API_PENDING:
      return {
        ...state,
        loading: true,
      };

    case API_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case API_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};
