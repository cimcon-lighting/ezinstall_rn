import { SETTING_ERROR, SETTING_SUCCESS } from '../action/action.types'

const initialState = {
  loading: true,
  data: '',
  error: ''
};

export default (state = initialState, action) => {

  switch (action.type) {

    case SETTING_SUCCESS:
      return {
        ...state,
        data: action.payload,
        loading: false
      };

    case SETTING_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload
      };

    default:
      return state;
  }
};
