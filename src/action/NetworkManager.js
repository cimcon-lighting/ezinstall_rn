import axios from 'axios';
import qs from 'query-string'
import {fetchSuccess, fetchError } from "./Action";

import { BASE_URL, SERVER_CONTEXT_PATH, } from '../utility/config'

export const networkManager = async (requestData) => {

    const { url, method, data } = requestData;

    const serverURL = `${SERVER_CONTEXT_PATH}${url}`
    console.log(`RequestURL : ${BASE_URL}${serverURL}`);
    console.log(`RequestParams : ${JSON.stringify(data)}`);

    const response = await axios.request({
        url: serverURL,
        method: method,
        baseURL: BASE_URL,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: qs.stringify(data),
    })
        .then(response => {
            console.log(`Response : ${JSON.stringify(response.data)}`);
            return fetchSuccess(response.data);
        })
        .catch(error => {
            console.error(`Response error : ${error}`);
            return fetchError(error);
        });

    return response;
};
