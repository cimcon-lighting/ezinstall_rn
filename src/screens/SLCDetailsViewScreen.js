import React, { useState, useEffect, Component } from 'react'

import {
    View,
    Text,
    StatusBar,
    StyleSheet,
    Modal,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native'

import { MAP_SHOW } from '../action/action.types'

import MapView from "react-native-map-clustering";
import { Marker } from "react-native-maps";

import ImageViewer from 'react-native-image-zoom-viewer';

import {
    NativeBaseProvider,
    HStack,
    Box,
    FlatList,
    VStack,
    Button
} from 'native-base'

import ProgressDialog from '../components/ProgressDialog'

import { connect, useDispatch } from 'react-redux'
import propTypes from 'prop-types'

import CustomHeader from '../layout/CustomHeader'

import Icon from 'react-native-vector-icons/Entypo';

import { SLC_DETAILS_TITLE } from '../utility/Constant'

import { STATUSBAR_HEIGHT } from '../utility/Utility'

import { getSLCDetails } from '../middleware/SLCDetailsMiddleware'

import SLCDetailsRaw from '../components/SLCDetailsRaw';

import { NOTES_TITLE } from '../utility/Constant'

 class SLCDetailsViewScreen extends Component {
    
        constructor(props) {
            super(props);

            const { item } = this.props.route.params

            this.state = {
                mapShow: false,
                imageURL: '',
                slcAssets: [],
                slcDetails: {},
                isimageView: false,
                isMapView: false,
                isLoading: false,
                slcID: item.slc_id,
                poleID: item.pole_id,
                ID: item.ID,
                macAddress: item.mac_address,
            };
        }

    componentDidMount() {
        this.getDetailsDataFromServer()
    }
     
     getDetailsDataFromServer = () => {
        console.log("=============================== BEGINING =========================")
         this.setState({
            isLoading : true
         }, () => {
            this.props.getSLCDetails({
                ID: this.state.ID,
            })
        })
    }
     
    render() {

        const { loading, data } = this.props.responseState
        
        const {
            mapShow,
            imageURL,
            slcAssets,
            slcDetails,
            isimageView,
            isMapView,
            isLoading,
            slcID,
            poleID,
            ID,
            macAddress } = this.state
        
        const { navigation } = this.props

        if (!loading) {
            this.props.responseState.loading = true

            const data1 = data.payload
            let tempAssets = ([...data1.data.Assets]) 
            const arrFinalAssets = ([])
                
            for (var i = 0; i < tempAssets.length; i++) {
                let assestsName = tempAssets[i].AttributeName
                if ((assestsName.toLowerCase() !== "pole image") &&
                    (assestsName.toLowerCase() !== "pole id") &&
                    (assestsName.toLowerCase() !== "notes")) {
                    arrFinalAssets.push(tempAssets[i])
                }
            }

            this.setState({
                isLoading: false,
                slcAssets: arrFinalAssets,
                slcDetails: {...data1.data}
            })
        }

        const mapButtonPress = () => {
            if (mapShow) {
                this.setState({
                    mapShow : false
                })
            } else {
                this.setState({
                    mapShow : true
                })
            } 
            console.log("aaya tyu ho click", mapShow)
        }

         return (
             <NativeBaseProvider>
                 <View >
                     <StatusBar translucent barStyle="light-content" />
                 </View>
                
                 <View style={{ marginTop: STATUSBAR_HEIGHT }}>
                     <CustomHeader
                         title={SLC_DETAILS_TITLE}
                         navigation={navigation}
                         onPress={mapButtonPress} />
                 </View>

                 <Box style={styles.topView}>
                     <HStack >
                         <Text style={styles.headingLable}> UID </Text>
                         <Text style={styles.collanLabel}>:</Text>
                         <Text style={styles.lable}>{macAddress}</Text>
                     </HStack>
                     <HStack >
                         <Text style={styles.headingLable}> SLC ID </Text>
                         <Text style={styles.collanLabel}>:</Text>
                         <Text style={styles.lable}>{slcID}</Text>
                         <Icon
                             name="chevron-down"
                             size={26}
                             color="white"
                         />
                     </HStack>
                     <HStack >
                         <Text style={styles.headingLable}> POLE ID </Text>
                         <Text style={styles.collanLabel}>:</Text>
                         <Text style={styles.lable}>{poleID}</Text>
                     </HStack>
                 </Box>
                 {
                     (!isLoading) && (
                         <View style={styles.container}>
                             <FlatList
                                 data={slcAssets}
                                 keyExtractor={(item, index) => index}
                                 renderItem={({ item, index, separators }) => (
                                     <SLCDetailsRaw item={item} />
                                 )}
                                 ListFooterComponent={
                                     <VStack>
                                         <HStack style={styles.forHstack}>
                                             <Text style={styles.assetsHeading}> Notes </Text>
                                             <TouchableWithoutFeedback onPress={() => {
                                                 navigation.navigate(NOTES_TITLE, {
                                                     poleOptions: slcDetails.pole_options,
                                                     other : "YOYO"
                                                 })
                                             }}>
                                                 <Text style={styles.assetLable}>View</Text>
                                             </TouchableWithoutFeedback>
                                         </HStack>
                                
                                         <HStack style={styles.forHstack}>
                                             <Text style={styles.assetsHeading}> Date of Installation </Text>
                                             <Text style={styles.assetLable}>{slcDetails.date_of_installation}</Text>
                                         </HStack>

                                         <HStack style={styles.forHstack}>
                                             <Text style={styles.assetsHeading}> Address </Text>
                                             <Text style={styles.assetLable}>{slcDetails.address}</Text>
                                         </HStack>

                                         <HStack style={{ justifyContent: 'center', margin: 10 }} space={25}>
                                             <Button
                                                 onPress={() => {
                                                     this.setState({
                                                         imageURL: [{ url: slcDetails.pole_image_url }],
                                                         isimageView : true
                                                     })
                                                     console.log("CAMERA PRESSED " + slcDetails.pole_image_url)
                                                 }}
                                                 style={{ borderRadius: 20, height: 40, width: 40 }}
                                                 bg="#01317B"
                                                 endIcon={<Icon
                                                     size={16}
                                                     name="camera"
                                                     color="white" />} />

                                             <Button
                                                 bg="#01317B"
                                                 style={{ borderRadius: 20, height: 40, width: 110 }}
                                                 onPress={() => { console.log("EDIT BUTTON PREESED") }}>
                                                 EDIT
                                             </Button>
                                         </HStack>
                                     </VStack>
                                 }
                             >
                             </FlatList>
                         </View>
                     )
                 }
                 {
                     (isimageView) && (
                         <Modal
                             visible={true}
                             transparent={true}>
                             <ImageViewer
                                 enableSwipeDown
                                 imageUrls={imageURL}
                                 onSwipeDown={() => this.setState({
                                     isimageView : false
                                 })}
                             />
                         </Modal>)
                 }
                 {
                  
                     (mapShow) && (
                         // console.log("LAT =================== " + slcDetails.lat + "LNG =================" + slcDetails.lng,)
                         <Modal
                             visible={true}
                             transparent={true}>
                             <TouchableWithoutFeedback onPress={() => {
                                 this.setState({
                                    mapShow : false
                                 }, () => { console.log("AFTERST SET", mapShow) })
                                 console.log("RST SET", mapShow)
                             }}>
                                 <View style={styles.childView}>
                                     <View style={{
                                         paddingTop: '20%',
                                         overflow: 'hidden',
                                         alignItems: 'center',
                                     }}>
                                         <MapView
                                             style={{
                                                 height: '90%',
                                                 width: '100%',
                                                 overflow: 'hidden',
                                                 justifyContent: 'center',
                                                 alignItems: 'center',
                                                 borderColor: '#0072BC',
                                                 borderRadius: 15,
                                                 borderWidth: 2,
                                             }}
                                             initialRegion={{
                                                 latitude: parseFloat(slcDetails.lat),
                                                 longitude: parseFloat(slcDetails.lng),
                                                 latitudeDelta: 0.0922,
                                                 longitudeDelta: 0.0421,
                                             }} >
                                             <Marker coordinate={{ latitude: parseFloat(slcDetails.lat), longitude: parseFloat(slcDetails.lng) }} />
                                               {/* <Marker coordinate={{ latitude: 23.032, longitude: 72.5714 }} /> */}
                                         </MapView>
                                         <Button
                                             bg="#01317B"
                                             style={{ borderRadius: 20, height: 40, width: 110, marginTop: -50 }}
                                             onPress={() => {
                                                 this.setState({
                                                    mapShow : false
                                                },() => {})
                                             }}>
                                             OK
                                         </Button>
                                     </View>
                                 </View>
                             </TouchableWithoutFeedback>
                         </Modal>
                     )
                 }
                 <ProgressDialog visible={isLoading} />
             </NativeBaseProvider>)
    }
}

const mapDispatchToProps = {
    getSLCDetails
}

const mapStateToProps = (state) => ({
    responseState: state.SLCDetailsReducers,
})

export default connect(mapStateToProps, mapDispatchToProps)(SLCDetailsViewScreen)

const styles = StyleSheet.create({
    topView : {
        flex: 0,
        backgroundColor: '#0072BC',
        padding: 12,
    },
    headingLable : {
        color : "#FFF",
        fontWeight : "bold",
        margin : 3,
        fontSize : 16,
        width : "25%"
    },
    collanLabel : {
        color : "#FFF",
        fontWeight : "bold",
        margin : 3,
        fontSize : 16,
        width : "3%"
    },
    lable : {
        color : "#FFF",
        fontWeight : 'normal',
        margin : 2,
        fontSize : 16,
        flex : 1
    },
    container: {
        backgroundColor: 'white',
        justifyContent: 'space-between',
        padding: 4,
        flex: 1,
    },
    assetsHeading: {
        color : "#222222",
        margin : 3,
        fontSize : 15,
        width : "50%"
    },
    assetLable: {
        color : "#585858",
        fontWeight : 'normal',
        margin : 2,
        fontSize : 15,
        flex: 1,
        textAlign: 'center',
    },
    forHstack :{
        flex : 1,
        margin : 8,
        justifyContent : 'space-around' 
    },
    childView: {
        justifyContent: 'center',
        // backgroundColor: 'transparent',
        backgroundColor : 'rgba(1, 1, 1, 0.7)',
        padding: 40,
        flex: 1,
        overflow : 'hidden',
    },
    text: {
        fontSize: 24,
        color:'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    map: {
        flex: 1
    }
})