import {
    NativeBaseProvider,
    Input,
    FormControl,
    HStack,
    Button,
    Select,
    CheckIcon,
    FlatList
} from 'native-base'

import React, { useEffect, useState, useCallback, Component } from 'react'

import CustomHeader from '../layout/CustomHeader'

import ProgressDialog from '../components/ProgressDialog'

import moment from 'moment';

import SLCListRaw from '../components/SLCListRaw';

import DateTimePicker from "react-native-modal-datetime-picker";

import {
    Text,
    StyleSheet,
    View,
    Keyboard,
    StatusBar,
    RefreshControl,
} from 'react-native'

import { connect } from 'react-redux'
import propTypes from 'prop-types'

import { getSLCList } from '../middleware/SLCListMiddleware'

import Icon from 'react-native-vector-icons/FontAwesome';

import { DismissKeyboardView, STATUSBAR_HEIGHT } from '../utility/Utility'

import { SLC_LIST_TITLE } from '../utility/Constant'

class SLCListScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            searchShow: false,
            searchButtonPressed: false,
            slcID: '',
            slcType: 'All',
            fromDate: moment(Date()).add(-6, 'days').format('MM-DD-YYYY'),
            toDate: moment(Date()).format('MM-DD-YYYY'),
            isFrmDate: true,
            isDatePickerVisible: false,
            slcList: [],
            totalRaw: 0,
            refreshing: false,
            isLoading: false,
            isCancelClick: false,
            pageNo : 1
        }
    }
//ondidmount api call

    componentDidMount() {
        this.getDataFromServer()
    }
    
    getDataFromServer = () => {
        console.log("=============================== BEGINING =========================")
        
        this.setState({
            refreshing: false,
            isLoading : true
        }, () => {
            this.props.getSLCList({
                pageNo: this.state.pageNo,
                fromDate: this.state.fromDate,
                toDate: this.state.toDate,
                slcLowerCase: this.state.slcType.toLowerCase(),
                slcID: this.state.slcID
            })
        })
    }
    
    onRefresh = () => {
         this.setState({
            refreshing: true 
         }, () => {
            this.cancelButton()
        })
        
    }

    cancelButton = () => {
        console.log("cancel button pressed")
        this.setState({
            slcID: '',
            isCancelClick : true,
            slcType: 'All',
            fromDate: moment(Date()).add(-6, 'days').format('MM-DD-YYYY'),
            toDate: moment(Date()).format('MM-DD-YYYY'),
            pageNo : 1,
            slcList : []
        }, () => {
            console.log("cancel button pressed ",this.state.slcID)
            this.getDataFromServer()
        })
    }

    loadMore = () => {
        console.log("====== ======== SLCLIST LENGTH and TOTALRAW " + this.state.slcList.length, this.state.totalRaw)
        if (this.state.slcList.length != this.state.totalRaw) {
            this.state.pageNo = this.state.pageNo + 1
            console.log(" PAGGINATION " + this.state.pageNo)
            this.getDataFromServer()
        }
    }

    hideDatePicker = () => {
        this.setState({
            isDatePickerVisible: false
        })
    };
        
    handleConfirm = (date) => {
        if (this.state.isFrmDate) {
            this.setState({
                fromDate: moment(date).format('MM-DD-YYYY')
            })
        } else {
            this.setState({
                toDate: moment(date).format('MM-DD-YYYY')
            })
        }
        this.hideDatePicker()
    };

    showDatePicker = (isFrmDt) => {
        Keyboard.dismiss()
        this.setState({
            isFrmDate: isFrmDt,
            isDatePickerVisible: true
        })
    };

     searchButton = () => {
                Keyboard.dismiss()
                const date1 = moment(this.state.fromDate, 'MM-DD-YYYY').valueOf()
                const date2 = moment(this.state.toDate, 'MM-DD-YYYY').valueOf()
        
                if (date1 > date2) {
                    alert("From date should not be greater than To date")
                } else {
                    this.setState({
                        slcList: [],
                        pageNo: 1
                    }, () => {
                        this.getDataFromServer()
                    })
                }
            }
    render() {

        const { navigation } = this.props

        const { loading, data } = this.props.responseState

        const { searchShow,
            searchButtonPressed,
            slcID,
            slcType,
            fromDate,
            toDate,
            isFrmDate,
            isDatePickerVisible,
            slcList,
            totalRaw,
            refreshing,
            isLoading,
            isCancelClick,
            pageNo } = this.state
        
        if (!loading) {
            console.log("RESPONSE", data.payload.data.tot_no_of_records)
            this.props.responseState.loading = true

            this.setState({
                slcList: [...slcList, ...data.payload.data.List],
                totalRaw: data.payload.data.tot_no_of_records,
                isLoading: false
            })
        }
        
        const searchButtonPress = () => {
            if (searchButtonPressed) {
                this.setState({
                    searchButtonPressed: false,
                    searchShow : false
                })
            } else {
                this.setState({
                    searchButtonPressed: true,
                    searchShow : true
                })
            } 
            console.log("aaya tyu ho click", searchShow)
        }
        
        return (
        <NativeBaseProvider>
        <View >
            <StatusBar translucent barStyle="light-content" />
        </View>
        <View style={{ paddingTop: STATUSBAR_HEIGHT }}>
            <CustomHeader
                title={SLC_LIST_TITLE}
                navigation={navigation}
                onPress={searchButtonPress} />
        </View>
        <View style={styles.componant}>
                {
                    (searchShow) && (
                     <DismissKeyboardView>

                     <View style={styles.searchView}>
 
                         <FormControl isRequired>
 
                             <Input mx={5} my={3}
                                 placeholder="Search by SLC ID"
                                 value={slcID}
                                 fontSize={15}
                                        onChangeText={(value) => {
                                        this.setState({
                                             slcID : value
                                         })
                                    }}
                                 color="#474a4d" />
                         </FormControl>
 
                         <FormControl isRequired >
                             <Select
                                 mx={5} my={2}
                                 selectedValue={slcType}
                                 accessibilityLabel="Choose SLC Type"
                                 placeholder="Choose SLC Type"
                                 fontSize={15}
                                 _selectedItem={{
                                     bg: "gray",
                                     endIcon: <CheckIcon size={5} />,
                                 }}
                                 mt="1"
                                 onValueChange={(itemValue) => setSlcType(itemValue)}
                             >
                                 <Select.Item label="All" value="All" />
                                 <Select.Item label="Internal" value="Internal" />
                                 <Select.Item label="External" value="External" />
                             </Select>
                         </FormControl>
 
                         <HStack style={{ justifyContent: "space-between" }} >
 
                             <Button
                                 onPress={() => this.showDatePicker(true)}
                                 w={{
                                     base: "39%"
                                 }}
                                 mx={5} my={1}
                                 variant="outline"
                                 backgroundColor="#FFF"
                                 borderRadius={5}
                                 borderWidth={1}
                                 borderColor="#e0e0e0"
                                 endIcon={<Icon
                                     style={styles.sendIcon}
                                     name="calendar"
                                     size={18}
                                     color="#c7c7c7"
                                 />}>
                                 <Text style={{ color: "#474a4d" }}>{fromDate}</Text>
                             </Button>
 
                             <Button
                                 onPress={() => this.showDatePicker(false)}
                                 w={{
                                     base: "39%"
                                 }}
                                 mx={5} my={1}
                                 variant="outline"
                                 backgroundColor="#FFF"
                                 borderRadius={5}
                                 borderWidth={1}
                                 borderColor="#e0e0e0"
                                 endIcon={<Icon
                                     style={styles.sendIcon}
                                     name="calendar"
                                     size={18}
                                     color="#c7c7c7"
                                 />}>
                                 <Text style={{ color: "#474a4d" }}>{toDate}</Text>
                             </Button>
 
                         </HStack>
 
 
                         <HStack style={{ justifyContent: 'center', margin: 10 }} space={25}>
                             <Button
                                 mode="contained"
                                 style={styles.button}
                                 size="md"
                                 w={{ base: "20%" }}
                                 onPress={ () => this.searchButton()}>
                                 Search
                             </Button>
 
                             <Button
                                 mode="contained"
                                 style={styles.button}
                                 size="md"
                                 w={{ base: "20%" }}
                                            onPress={() => this.cancelButton()}>
                                 Cancel
                             </Button>
                         </HStack>
 
                     </View>
                    </DismissKeyboardView>
                )
            }
            <View style={styles.container}>
                <FlatList
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={this.onRefresh}
                        />
                    }
                    data={slcList}
                    keyExtractor={(item) => item.ID}
                    renderItem={({ item, index, separators }) => (
                        <SLCListRaw item={item} navigation={navigation} />
                    )}
                    onEndReached={this.loadMore}
                >
                </FlatList>
            </View>

            <DateTimePicker
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={this.handleConfirm}
                onCancel={this.hideDatePicker}
                maximumDate={new Date()}
            />

        </View>
        <ProgressDialog visible={isLoading} />
    </NativeBaseProvider>)
    }
}

// const SLCListScreen = ({ getSLCList, searchViewShow, navigation, responseState }) => {

//     const [slcID, setSlcID] = useState('')
//     const [slcType, setSlcType] = useState('All')
//     const [fromDate, setFromDate] = useState(moment(Date()).add(-6, 'days').format('MM-DD-YYYY'))
//     const [toDate, setToDate] = useState(moment(Date()).format('MM-DD-YYYY'))
//     const [isFrmDate, setisFrmDate] = useState(true)
//     const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
//     const [slcList, setSlcList] = useState([])
//     const [totalRaw, settotalRaw] = useState(0)
//     const [refreshing, setRefreshing] = useState(false);
//     const [isLoading, setIsLoading] = useState(false)

//     var isCancelClick = false

//     var pageNo = 1

//     const showDatePicker = (isFrmDt) => {
//         Keyboard.dismiss()
//         setisFrmDate(isFrmDt)
//         setDatePickerVisibility(true);
//     };

//     const onRefresh = useCallback(() => {
//         setRefreshing(true);
//         cancelButton()
//     }, []);

//     const hideDatePicker = () => {
//         setDatePickerVisibility(false);
//     };

//     const handleConfirm = (date) => {
//         if (isFrmDate) {
//             setFromDate(moment(date).format('MM-DD-YYYY'))
//         } else {
//             setToDate(moment(date).format('MM-DD-YYYY'))
//         }
//         hideDatePicker();
//     };

//     const searchButton = () => {
//         Keyboard.dismiss()
//         const date1 = moment(fromDate, 'MM-DD-YYYY').valueOf()
//         const date2 = moment(toDate, 'MM-DD-YYYY').valueOf()

//         if (date1 > date2) {
//             alert("From date should not be greater than To date")
//         } else {
//             pageNo = 1
//             setSlcList([])
//             getDataFromServer(pageNo, fromDate, toDate, slcType, slcID)
//         }
//     }

//     const cancelButton = () => {
//         isCancelClick = true
//         Keyboard.dismiss()
//         setSlcType('All')
//         let frmDT = setFromDate(moment(Date()).add(-6, 'days').format('MM-DD-YYYY'))
//         let toDt = setToDate(moment(Date()).format('MM-DD-YYYY'))
//         pageNo = 1
//         setSlcID('')
//         setSlcList([])
//         getDataFromServer(pageNo, frmDT, toDt, "All", "")
//     }

//     const getDataFromServer = (page_No, from_Date, to_Date, slc_Type, slc_ID) => {
//         console.log("=============================== BEGINING =========================")
//         const slcLowerCase = slc_Type.toLowerCase()
//         setRefreshing(false);
//         setIsLoading(true)
//         getSLCList({ page_No, from_Date, to_Date, slcLowerCase, slc_ID })
//     }

//     const loadMore = () => {
//         console.log("====== ======== SLCLIST LENGTH and TOTALRAW " + slcList.length, totalRaw)
//         if (slcList.length != totalRaw) {
//             pageNo = pageNo + 1
//             console.log(" PAGGINATION " + pageNo)

//             getDataFromServer(pageNo, fromDate, toDate, slcType, slcID)
//         }
//     }

//     useEffect(() => {
//         if ((isCancelClick) && (slcID === '')) {
//             isCancelClick = false
//             getDataFromServer(pageNo, fromDate, toDate, slcType, slcID)
//         }
//     }, [slcID])

//     useEffect(() => {
//         getDataFromServer(pageNo, fromDate, toDate, slcType, slcID)
//     }, [])


//     useEffect(() => {
//         if (!responseState.loading) {
//             setIsLoading(false)
//             const data = responseState.data.payload
//             setSlcList([...slcList, ...data.data.List])
//             settotalRaw(data.data.tot_no_of_records)
//         }
//     }, [responseState])

//     return (
//         <NativeBaseProvider>
//             <View >
//                 <StatusBar translucent barStyle="light-content" />
//             </View>
//             <View style={{ paddingTop: STATUSBAR_HEIGHT }}>
//                 <CustomHeader
//                     title={SLC_LIST_TITLE}
//                     navigation={navigation}
//                     onPress={() => { console.log("aaya tyu ho click") }} />
//             </View>
//             <View style={styles.componant}>
//                 {
//                     (searchViewShow.isOpen) && (
//                         <DismissKeyboardView>

//                             <View style={styles.searchView}>

//                                 <FormControl isRequired>

//                                     <Input mx={5} my={3}
//                                         placeholder="Search by SLC ID"
//                                         value={slcID}
//                                         fontSize={15}
//                                         onChangeText={(value) => setSlcID(value)}
//                                         color="#474a4d" />
//                                 </FormControl>

//                                 <FormControl isRequired >
//                                     <Select
//                                         mx={5} my={2}
//                                         selectedValue={slcType}
//                                         accessibilityLabel="Choose SLC Type"
//                                         placeholder="Choose SLC Type"
//                                         fontSize={15}
//                                         _selectedItem={{
//                                             bg: "gray",
//                                             endIcon: <CheckIcon size={5} />,
//                                         }}
//                                         mt="1"
//                                         onValueChange={(itemValue) => setSlcType(itemValue)}
//                                     >
//                                         <Select.Item label="All" value="All" />
//                                         <Select.Item label="Internal" value="Internal" />
//                                         <Select.Item label="External" value="External" />
//                                     </Select>
//                                 </FormControl>

//                                 <HStack style={{ justifyContent: "space-between" }} >

//                                     <Button
//                                         onPress={() => showDatePicker(true)}
//                                         w={{
//                                             base: "39%"
//                                         }}
//                                         mx={5} my={1}
//                                         variant="outline"
//                                         backgroundColor="#FFF"
//                                         borderRadius={5}
//                                         borderWidth={1}
//                                         borderColor="#e0e0e0"
//                                         endIcon={<Icon
//                                             style={styles.sendIcon}
//                                             name="calendar"
//                                             size={18}
//                                             color="#c7c7c7"
//                                         />}>
//                                         <Text style={{ color: "#474a4d" }}>{fromDate}</Text>
//                                     </Button>

//                                     <Button
//                                         onPress={() => showDatePicker(false)}
//                                         w={{
//                                             base: "39%"
//                                         }}
//                                         mx={5} my={1}
//                                         variant="outline"
//                                         backgroundColor="#FFF"
//                                         borderRadius={5}
//                                         borderWidth={1}
//                                         borderColor="#e0e0e0"
//                                         endIcon={<Icon
//                                             style={styles.sendIcon}
//                                             name="calendar"
//                                             size={18}
//                                             color="#c7c7c7"
//                                         />}>
//                                         <Text style={{ color: "#474a4d" }}>{toDate}</Text>
//                                     </Button>

//                                 </HStack>


//                                 <HStack style={{ justifyContent: 'center', margin: 10 }} space={25}>
//                                     <Button
//                                         mode="contained"
//                                         style={styles.button}
//                                         size="md"
//                                         w={{ base: "20%" }}
//                                         onPress={() => searchButton()}>
//                                         Search
//                                     </Button>

//                                     <Button
//                                         mode="contained"
//                                         style={styles.button}
//                                         size="md"
//                                         w={{ base: "20%" }}
//                                         onPress={() => cancelButton()}>
//                                         Cancel
//                                     </Button>
//                                 </HStack>

//                             </View>
//                         </DismissKeyboardView>
//                     )
//                 }
//                 <View style={styles.container}>
//                     <FlatList
//                         refreshControl={
//                             <RefreshControl
//                                 refreshing={refreshing}
//                                 onRefresh={onRefresh}
//                             />
//                         }
//                         data={slcList}
//                         keyExtractor={(item) => item.ID}
//                         renderItem={({ item, index, separators }) => (
//                             <SLCListRaw item={item} navigation={navigation} />
//                         )}
//                         onEndReached={loadMore}
//                     >
//                     </FlatList>
//                 </View>

//                 <DateTimePicker
//                     isVisible={isDatePickerVisible}
//                     mode="date"
//                     onConfirm={handleConfirm}
//                     onCancel={hideDatePicker}
//                     maximumDate={new Date()}
//                 />

//             </View>
//             <ProgressDialog visible={isLoading} />
//         </NativeBaseProvider>
//     )
// }

const mapDispatchToProps = {
    getSLCList
}

const mapStateToProps = (state) => ({
    responseState: state.SLCListReducers,
})

export default connect(mapStateToProps, mapDispatchToProps)(SLCListScreen)

const styles = StyleSheet.create({
    searchView: {
        backgroundColor: "#FFF",
        width: '100%'
    },
    componant: {
        flex: 1,
    },
    sendIcon: {
        flexDirection: 'row-reverse',
        marginRight: 27,
    },
    button: {
        borderRadius: 5,
        backgroundColor: "#0072BC"
    },
    dateContainer: {
        alignSelf: "flex-end",
        marginRight: 30
    },
    container: {
        backgroundColor: '#0072BC',
        justifyContent: 'space-between',
        padding: 4,
        flex: 1,
    }
})