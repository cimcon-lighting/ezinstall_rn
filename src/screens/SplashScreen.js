
import React from "react";
import { ImageBackground, StyleSheet, View } from "react-native";
import { NativeBaseProvider, Center, Image } from "native-base";

//Images -> from ./assets/img
import AppLogo from '../assets/img/logo.jpg';
import SplashBG from '../assets/img/splash_screen.jpg';


const SplashScreen = () => {

    return (
        <NativeBaseProvider>
            <View style={styles.container}>
                <ImageBackground
                    source={SplashBG}
                    resizeMode="cover"
                    style={styles.image}
                >
                    <Center>
                        <Image
                            source={AppLogo}
                            style={styles.app_logo}
                            resizeMode="contain"
                            alt="AppLogo"
                        />
                    </Center>
                </ImageBackground>
            </View>
        </NativeBaseProvider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
    },
    image: {
        flex: 1,
        justifyContent: "center",
    },
    app_logo: {
        width: 120,
        height: 120,
    },
});

export default SplashScreen;