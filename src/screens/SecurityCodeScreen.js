import React, { useState, useEffect } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Platform,
  NativeModules,
  StatusBar
} from "react-native";
import {
  VStack,
  Input,
  NativeBaseProvider,
  Center,
  Button,
  Text,
  FormControl,
  Image,
} from "native-base";

import { connect , useDispatch } from "react-redux";

import SplashBG from "../assets/img/bg_sky_blue.png";

import {
  setClientDetails,
  setStorageKey,
  getPlatform
} from "../utility/Utility";
//import { requestPermission } from "../utility/AskPermission";
import Icon from "react-native-vector-icons/Ionicons";
import IMEI from "react-native-imei";

import propTypes from "prop-types";
import { doSecurity } from "../middleware/SecurityMiddleware";
import EmptyContainer from "../components/EmptyContainer";
import ProgressDialog from '../components/ProgressDialog'
import {
  KEY_SECURITY_CODE,
  IS_SECURITY_CODE_EXIST,
  KEY_UNIQUE_DEVICE_ID,
} from "../utility/Constant";
import { SECURITY_SUCCESS_NAV } from "../action/action.types";

const SecurityCodeScreen = ({
  doSecurity,
  navigation,
  responseState,
}) => {

  const dispatch = useDispatch();
  const [code, setCode] = useState("882843");
  const [uniqueId, setUniqueId] = useState("");

  const [passwordVisibleIcon, setPasswordVisibleIcon] = useState("eye-off");
  const [isPasswordVisible, setIsPasswordVisible] = useState(true);
  const [isValidSecurityCode, setIsValidSecurityCode] = useState(false);
  const [isLoading, setIsLoading] = useState(false)

   // useEffect(() => {
  //   requestPermission();  
  // }, []);

  useEffect(() => {
    if (!responseState.loading) {

      setIsLoading(false)
      const data = responseState.data.payload;

      console.log(`status : ${data.status}`);
      if (data.status === "success") {
        //store response in AsynStorage
        console.log(`In side status : ${data.status}`);
        setClientDetails(data);
        
        setStorageKey(IS_SECURITY_CODE_EXIST, 'true')
        setStorageKey(KEY_SECURITY_CODE, code)
        redirectLogin()

      } else if (data.status === "error" || data.status === "0") {
        alert(data.msg);
      }
      console.log(`SecurityScreen : ${data.msg}`);
    }
  }, [responseState]);

  const redirectLogin = () => {

    console.log("redirect login");
    const data = {
      type: SECURITY_SUCCESS_NAV,
      payload: true
    }
  
    dispatch(data)
  }

  const chnagePasswordVisibleState = () => {
    if (isPasswordVisible) {
      setIsPasswordVisible(false);
      setPasswordVisibleIcon("eye");
    } else {
      setIsPasswordVisible(true);
      setPasswordVisibleIcon("eye-off");
    }
  };

 

  const doSecurityCall = () => {
    if (isValidate(code)) {

      setIsLoading(true)
      //Api call
      console.log("Api is calling");

      //identify platform
      //const source = Platform.OS === "ios" ? "IOS" : "Android";
      const source = getPlatform()

      //identify current language locale
      const language =
        Platform.OS === "ios"
          ? NativeModules.SettingsManager.settings.AppleLocale ||
          NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13
          : NativeModules.I18nManager.localeIdentifier;
      console.log(language); //en_US

      doSecurity({ code, source, language ,uniqueId});
    } else {
      console.log("User doesn't enter security code");
      alert("Please enter security code !");
    }
  };

  const isValidate = (code) => {
    let isValidate = false;
    if (code == "") {
      isValidate = false;
      setIsValidSecurityCode(true);
    } else {
      isValidate = true;
      setIsValidSecurityCode(false);
    }
    return isValidate;
  };


  return (
    <NativeBaseProvider>
      <View >
        <StatusBar translucent barStyle="light-content" />
      </View>
      <View style={styles.container}>
        <ImageBackground
          source={SplashBG}
          resizeMode="cover"
          style={styles.image}
        >
          <Center>
            <VStack space={4} w="100%" alignItems="center">
              <Text style={styles.title} fontSize="xl">
                Security Code
              </Text>

              <FormControl isInvalid={isValidSecurityCode} w={"75%"}>
                <Input
                  color="#fff"
                  size="xl"
                  type="password"
                  InputRightElement={
                    <Icon
                      style={{ paddingLeft: 10, paddingRight: 10 }}
                      onPress={chnagePasswordVisibleState}
                      size={25}
                      color="#fff"
                      name={passwordVisibleIcon}
                    />
                  }
                  secureTextEntry={isPasswordVisible}
                  value={code}
                  onChangeText={(text) => setCode(text)}
                  placeholder="Enter security code"
                  placeholderTextColor="#d2d2d2"
                  selectionColor="#d2d2d2"
                />
                <FormControl.ErrorMessage
                _invalid={{
                  _text: {
                    color: "red.400",
                    underline: true,
                  },
                }}>
                  Please enter security code
                </FormControl.ErrorMessage>
              </FormControl>

              <Button
                style={styles.button}
                size="lg"
                colorScheme="green"
                w={"75%"}
                onPress={doSecurityCall}
              >
                NEXT
              </Button>
            </VStack>
          </Center>
        </ImageBackground>
      </View>
      <ProgressDialog visible={isLoading} />
    </NativeBaseProvider>
  );
};


SecurityCodeScreen.prototype = {
  doSecurity: propTypes.func.isRequired,
  responseState: propTypes.object.isRequired,
};

const mapDispatchToProps = {
  doSecurity: (data) => doSecurity(data),
};

const mapStateToProps = (state) => ({
  responseState: state.SecurityCodeReducers,
});

export default connect(mapStateToProps, mapDispatchToProps)(SecurityCodeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  image: {
    flex: 1,
    justifyContent: "center",
  },
  button: {
    color: "#50B848",
    fontSize: 15,
  },
  title: {
    color: "#fff",
  },
});

    //"react-native-imei": "^0.2.0",
 
   // const getDeviceIMEI = () => {
  //   //const IMEI = require("react-native-imei");
  //   setImei(IMEI.getImei());
  //   console.log("***" + imei);
  // };