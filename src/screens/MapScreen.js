import React, { useRef, useEffect, useState } from 'react'

import {
    StyleSheet,
    View,
    StatusBar,
    Dimensions
}
    from 'react-native'

import {
    NativeBaseProvider,
    Image,
    Text,
    VStack,
    HStack,

} from 'native-base'

import CustomHeader from '../layout/CustomHeader'

import MapView from "react-native-map-clustering";
import { Marker, PROVIDER_GOOGLE, PlaceCard, Callout,PROVIDER_DEFAULT } from "react-native-maps";
import { MAP_TITLE, SLC_DETAILS_TITLE,KEY_MAP_TYPE } from '../utility/Constant';
import { getLocation, getStorageKey, getClientDetails, getPlatform } from '../utility/Utility'
import { doSLCLatLong } from '../middleware/MapMiddleware'

import { connect, useDispatch } from "react-redux"
import propTypes from "prop-types";

import ProgressDialog from '../components/ProgressDialog'
import {KEY_IS_APPLE_MAP_TYPE_SELECTED} from '../utility/Constant';

const MapScreen = ({ doSLCLatLong, navigation, responseState }) => {
    const mapRef = useRef(null);

    const [kilometer, setKilometer] = useState(1.0)
    const [clientid, setClientid] = useState("")
    const [userid, setUserid] = useState("")
    const [top_lat, setTopLat] = useState(23.3672777120346)
    const [top_lon, setTopLon] = useState(72.69967440515757)
    const [bottom_lat, setBottomLat] = useState(22.88680205988556)
    const [bottom_lon, setBottomLon] = useState(72.4030066654086)

    const [mapType, setMapType] = useState("standard") //satellite, hybrid,standard
    const [isLoading, setIsLoading] = useState(false)
    const [slcList, setSlcList] = useState([])

    const [aspectRatio, setAspectRatio] = useState()
    const [lattitudeDelta, setLattitudeDelta] = useState(0.10)
    const [longitudeDelta, setLongitudeDelta] = useState(0.10)
    const [Curlatitude, setlattitude] = useState(23.1069) //23.10777 
    const [Curlongitude, setlongitude] = useState(72.5565)//72.55662
    const [provider,setProvider] = useState(PROVIDER_DEFAULT)

    const [region, setRegion] = useState({
        latitude: Curlatitude,
        longitude: Curlongitude,
        latitudeDelta: lattitudeDelta,
        longitudeDelta: longitudeDelta,
    })

    useEffect(() => {
        getUserInfo()
    }, [])

    useEffect(() => {
        if (!responseState.loading) {
            setIsLoading(false)
            const data = responseState.data.payload;
            console.log(`status : ${data.status}`);

            if (data.status === "1") {
                console.log("*** Map api succefully called******")
                setSlcList([...data.data.List])
            } else if (data.status === "error" || data.status === "0") {
                alert(data.msg);
            }
        }

    }, [responseState]);


    const mapMarkers = () => {
        return slcList &&
            slcList.map((item) => (
                <Marker
                    key={item.ID}
                    coordinate={{ latitude: parseFloat(item.lat), longitude: parseFloat(item.lng) }}
                    title={"SLC ID: " + item.slc_id}
                    description={"POLE ID: " + item.pole_id}
                    onCalloutPress={() => {
                        //navigate to SLCDetailsScreen here
                        console.log("###")
                        navigation.navigate(SLC_DETAILS_TITLE, {
                            item: item
                        })
                    }}
                >
                </Marker>
            ))
    }

    const getUserInfo = async () => {
        const { latitude, longitude } = await getLocation()
        setlattitude(parseFloat(latitude))
        setlongitude(parseFloat(longitude))

        const { userid, ClientID } = await getClientDetails()
        setClientid(ClientID)
        setUserid(userid)

        console.log("***" + latitude + ": " + longitude + ", Client Id:" + clientid + " Userid:" + userid)

        //mapType from AyncStorage:
        const mapType = await getStorageKey(KEY_MAP_TYPE)
        setMapType(mapType)

        setLattitudeDelta(0.10)
        setLongitudeDelta(0.10)

        setRegion({
            latitude: parseFloat(latitude),
            longitude: parseFloat(longitude),
            latitudeDelta: lattitudeDelta,
            longitudeDelta: longitudeDelta,
        })

            if(getPlatform()==="IOS"){
                if(await getStorageKey(KEY_IS_APPLE_MAP_TYPE_SELECTED)==="true"){
                    setProvider(PROVIDER_DEFAULT)
                }else{
                    setProvider(PROVIDER_GOOGLE)
                }
            }

        //API calling
        doApiCall(userid, ClientID, latitude, longitude)
    }

    const doApiCall = (userid, clientid, latitude, longitude) => {
        console.log("API is calling");
        setIsLoading(true)
        doSLCLatLong({
            latitude, longitude, kilometer, clientid, userid, top_lat, top_lon, bottom_lat, bottom_lon
        })
    }

    return (
        <NativeBaseProvider>
            <View >
                <StatusBar translucent barStyle="light-content" />
            </View>

            <View style={{ marginTop: StatusBar.currentHeight }}>
                <CustomHeader
                    title={MAP_TITLE}
                    navigation={navigation} />
            </View>
            <ProgressDialog visible={isLoading} />
            <MapView initialRegion={region}
                style={{ flex: 1 }}
                showsUserLocation={true}
                zoomEnabled={true}
                mapType={mapType}
                zoomControlEnabled={true}
                provider={provider}
                onRegionChange={
                    console.log("onRegionChange")
                    // region
                }
                onRegionChangeComplete={
                    console.log("onRegionChangeComplete")
                }>
                {mapMarkers()}
            </MapView>
        </NativeBaseProvider>

    )
}


const styles = StyleSheet.create({
    componant: {
        flex: 1,
        alignItems: "center",
        justifyContent: 'center',
    },
    map: {
        flex: 1
    },
    calloutTitle: {
        fontSize: 17,
        marginBottom: 5,
        fontWeight: "bold"
    },
    calloutDescription: {
        fontSize: 14
    }
})

MapScreen.prototype = {
    doSLCLatLong: propTypes.func.isRequired,
    responseState: propTypes.object.isRequired,
};

const mapDispatchToProps = {
    doSLCLatLong: (data) => doSLCLatLong(data),
};

const mapStateToProps = (state) => ({
    responseState: state.MapReducers,
});

export default connect(mapStateToProps, mapDispatchToProps)(MapScreen);


{/* <HStack alignItems="center" space={2} >
<Image
    source={require('../assets/img/pole_blue.png')}
    style={{ width: 25, height: 40 }}
    alt="PoleImage"
/>
<VStack>
    <Text style={styles.calloutTitle}>SLC ID: {item.slc_id}</Text>
    <Text style={styles.calloutDescription}>POLE ID: {item.pole_id}</Text>
</VStack>
</HStack> */}


 {/* <Callout
                        onPress={() => {
                            console.log("@@@")
                            navigation.navigate(SLC_DETAILS_TITLE, {
                                item: item
                            })
                        }}>
                        <View>
                            <HStack alignItems="center" space={2} >
                                <Image
                                    source={require('../assets/img/pole_blue.png')}
                                    style={{ width: 25, height: 40 }}
                                    alt="PoleImage"
                                />
                                <VStack>
                                    <Text style={styles.calloutTitle}>SLC ID: {item.slc_id}</Text>
                                    <Text style={styles.calloutDescription}>POLE ID: {item.pole_id}</Text>
                                </VStack>
                            </HStack>
                        </View>
                    </Callout> */}

                    {/* 
                    //Custome image
                    <Image
                        source={require('../assets/img/custom_pin.png')}
                        style={{ width: 40, height: 45 }}
                        resizeMode="contain"
                        alt="temp"
                    /> */}