import * as React from 'react';
import { Text, View, Image, StyleSheet, SafeAreaView } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import TabsettingIcon from 'react-native-vector-icons/dist/Octicons';
import TablistIcon from 'react-native-vector-icons/dist/Feather'
import TabscanIcon from 'react-native-vector-icons/dist/AntDesign'
import TabMapIcon from 'react-native-vector-icons/dist/FontAwesome5'

import { createStackNavigator } from '@react-navigation/stack';

import CustomHeader from '../layout/CustomHeader'

import SlcList from './SLCListScreen'
import Setting from './SettingScreen'
import Scan from './ScanScreen'
import SLCDetailsViewScreen from './SLCDetailsViewScreen'
import SLCScanScreen from './SLCScanScreen';
import NotesScreen from './NotesScreen';
import { NOTES_TITLE, SLC_DETAILS_TITLE, SLC_LIST_TITLE, MAP_TITLE } from '../utility/Constant'
import MapScreen from './MapScreen';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const SLCListStack = createStackNavigator();
const SLCScanStack = createStackNavigator();
const MapScreenStack = createStackNavigator();
const SettingScreen=createStackNavigator();

function SLCListScreens() {
  return (
    <SLCListStack.Navigator >
      {/* <SLCListStack.Screen name={NOTES_TITLE} component={NotesScreen} options={{ headerShown: false }} /> */}
      <SLCListStack.Screen name={SLC_LIST_TITLE} component={SlcList} options={{ headerShown: false }} />
      <SLCListStack.Screen name={SLC_DETAILS_TITLE} component={SLCDetailsViewScreen} options={{ headerShown: false }} />
      <SLCListStack.Screen name={NOTES_TITLE} component={NotesScreen} options={{ headerShown: false }} />
    </SLCListStack.Navigator>
  );
}

function SLCScanScreens() {
  return (
    <SLCScanStack.Navigator>
      <SLCScanStack.Screen name='Scan' component={Scan} options={{ headerShown: false }} />
      <SLCScanStack.Screen name='SLC Scan' component={SLCScanScreen} options={{ headerShown: false }} />
    </SLCScanStack.Navigator>
  )
}

function MapScreens() {
  return (
    <MapScreenStack.Navigator>
      <MapScreenStack.Screen name={MAP_TITLE} component={MapScreen} options={{ headerShown: false }} />
      <MapScreenStack.Screen name={SLC_DETAILS_TITLE} component={SLCDetailsViewScreen} options={{ headerShown: false }} />
    </MapScreenStack.Navigator>
  )

}

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="SlcList"
      screenOptions={{
        tabBarActiveTintColor: 'white',
        tabBarStyle: { backgroundColor: "#01317B" },
      }}
    >
      <Tab.Screen
        name="SlcList"
        component={SLCListScreens}
        options={{
          headerShown: false,
          tabBarLabel: 'LIST',
          // header: (props) => <CustomHeader {...props} />,
          tabBarIcon: ({ color }) => (
            <TablistIcon name="list" size={26} color={color} />
            // <Image source={require('../assets/tabbarIcons/tabList.png')} 
            //         style={styles.tabIcon} 
            //         tintColor={color}/>
          ),
        }}
      />
      <Tab.Screen
        name="SLCScan"
        component={SLCScanScreens}
        options={{
          headerShown: false,
          tabBarLabel: 'SCAN',
          tabBarIcon: ({ color }) => (
            <TabscanIcon name="qrcode" size={26} color={color} />
            // <Image source={require('../assets/tabbarIcons/tabList.png')} 
            //         style={styles.tabIcon} 
            //         tintColor={color}/>
          ),
        }}
      />
      <Tab.Screen
        name="Map"
        component={MapScreens}
        options={{
          tabBarLabel: 'MAP',
          headerShown: false,
          tabBarIcon: ({ color }) => (
            <TabMapIcon name="map-marked-alt" size={26} color={color} />
            // <Image source={require('../assets/tabbarIcons/tabMap.png')} 
            //         style={styles.tabIcon} 
            //         tintColor={color}/>
          ),
        }}
      />

      <Tab.Screen
        name="Setting"
        component={Setting}

        options={{
          tabBarLabel: 'SETTINGS',
          headerShown: false,
          tabBarIcon: ({ color }) => (
            <TabsettingIcon name="gear" size={26} color={color} />
            // <Image source={require('../assets/tabbarIcons/tabSetting.png')} 
            //         style={styles.tabIcon} 
            //         tintColor={color}/>
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default Tabbar = () => {
  return (
    <MyTabs />
  );
}

const styles = StyleSheet.create({
  tabIcon: {
    width: 26, height: 26
  },
  container: {
    flex: 1,
  },
})
