import React, { Component} from 'react'

import {
    Text,
    View,
    StatusBar,
    StyleSheet,
} from 'react-native'

import {
    Box,
    NativeBaseProvider,
    TextArea,
    FlatList,
} from 'native-base'

import { NOTES_TITLE } from '../utility/Constant'

import { STATUSBAR_HEIGHT } from '../utility/Utility'

import PoleOptions from '../components/PoleOptions'

import CustomHeader from '../layout/CustomHeader'

class NotesScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            poleOptions: []
        };
    }

    render() {

        const { slcAssets } = this.state

        const { navigation } = this.props

        const { poleOptions, other } = this.props.route.params

    return (
        <NativeBaseProvider>
            <View >
                <StatusBar translucent barStyle="light-content" />
            </View>

            <View style={{ marginTop: STATUSBAR_HEIGHT }}>
                <CustomHeader title={NOTES_TITLE} navigation={navigation} />
            </View>

            <View style={styles.mainContainer}>
                <Box style={styles.box}>
                    <Text style={styles.text}> Enter Notes </Text>
                    <TextArea
                        h={20}
                        bg="white"
                        placeholder="Enter notes here"
                    />
                </Box>
            </View>
            
            <View style={styles.container}>
                <Text style={{...styles.text, paddingLeft : 20}}>Devices</Text>
                <FlatList
                    data={poleOptions}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index, separators }) => (
                        <PoleOptions item={item} />
                    )} />
            </View>

        </NativeBaseProvider>
    )
    }
}

export default NotesScreen;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 0,
        backgroundColor: "#0072BC"
    },
    container: {
        backgroundColor: "#0072BC",
        justifyContent: 'space-between',
        padding: 4,
        flex: 1,
    },
    text : {
        paddingBottom: 10,
        fontSize : 16,
        color: "white",
    },
    box: {
        flex: 0,
        padding: 20,
    }
})