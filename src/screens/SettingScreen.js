import React, { useState, useEffect } from 'react'
import { Text, StyleSheet, Alert, StatusBar, Platform } from "react-native";
import { clearAllData, getClientDetails, getPlatform, getStorageKey, setisLoggedIn, setStorageKey } from "../utility/Utility";
import { connect, useDispatch } from 'react-redux'
import { LOGIN_SUCCESS_NAV, SECURITY_SUCCESS_NAV,LOGOUT_NAV } from "../action/action.types";
import { Button, HStack, VStack, NativeBaseProvider, View, Stack, Switch, Image, Select, CheckIcon, } from 'native-base'
import CustomHeader from '../layout/CustomHeader'
import {
  KEY_MAP_TYPE,
  KEY_LANGUAGE_TYPE,
  SETTING_TITLE,
  MAP_STANDARD,
  MAP_HYBRID,
  MAP_SATELLITE,
  MAP_STANDARD_IOS,
  MAP_HYBRID_IOS,
  MAP_SATELLITE_IOS,
  KEY_IS_APPLE_MAP_TYPE_SELECTED,
  LANGUAGE_CODE_PORTUGUES,
  LANGUAGE_CODE_SPANISH,
  LANGUAGE_CODE_ENGLISH,
  KEY_MEASURMENT_UNIT,
  UNIT_METIRC,
  UNIT_ENGLISH,
  IS_LOGGED_IN,
} from '../utility/Constant';
import { getAppVersion } from '../utility/Utility'
import SegmentedControlTab from 'react-native-segmented-control-tab';
import propTypes from 'prop-types'
import { getSettingData, sendLanguageData } from '../middleware/SettingMiddleware'
import { setSetting } from '../utility/Utility'
import ProgressDialog from '../components/ProgressDialog'

const Setting = ({ navigation, responseState, navigtionState, getSettingData, responseLanguageState, sendLanguageData }) => {

  const [version, setVersion] = useState("")
  const [selectedIndex, setSelectedIndex] = useState(0)
  const [language_type, setLanguageType] = useState("")//en
  const [maptype, setMaptype] = useState("") //standard
  const [isLoading, setIsLoading] = useState(false)

  const toggleSwitchPole = () => setPoleIdChecked(previousState => !previousState);
  const [isPoleIdChecked, setPoleIdChecked] = useState(false)
  const [isAssetView, setAssetViewChecked] = useState(false)
  const [isPoleDisable, setPoleDisable] = useState("false")
  const [isAssetViewDisable, setAssetViewDisable] = useState("false")

  const dispatch = useDispatch();

  useEffect(() => {
    try {

      //App version:
      const appVersion = getAppVersion()
      setVersion(appVersion)

      //Get data from local storage
      getDataFromLocal()

      //Api - get data from setting api
      callSettingApi()

    } catch (e) {
      console.log(e)
    }
  }, [])

  useEffect(() => {
    if (!responseState.loading) {
      setIsLoading(false)
      const data = responseState.data.payload;

      console.log(`status : ${data.status}`);

      if (data.status === "success") {
        //store response in AsynStorage
        console.log(`In side status : ${data.status}`);
        setSetting(data);

        const isPoleID = data.data.client_slc_pole_assets_view
        const isAssetView = data.data.client_slc_pole_assets_view

        if (isPoleID === "Yes")
          setPoleIdChecked(true)
        else
          setPoleIdChecked(false)

        if (isAssetView === "Yes")
          setAssetViewChecked(true)
        else
          setAssetViewChecked(false)

        console.log("###" + data.data.client_slc_pole_assets_view);

      } else if (data.status === "error" || data.status === "0") {
        alert(data.msg);
      }
    }

  }, [responseState])

  useEffect(() => {

    if (!responseLanguageState.loading) {
      setIsLoading(false)
      const data = responseState.data.payload;

      console.log(`status : ${data.status}`);

      if (data.status === "success") {
        //store response in AsynStorage
        console.log(`In side status of change lang api : ${data.status}`);

        //Change enter app's language here
        alert("Language changed successfully")

      } else if (data.status === "error" || data.status === "0") {
        alert(data.msg);
      }
    }

  }, [responseLanguageState])

  const getDataFromLocal = async () => {

    //Measurment unit
    const unit = await getStorageKey(KEY_MEASURMENT_UNIT);
    console.log("###" + unit)
    if (unit === UNIT_ENGLISH) {
      setSelectedIndex(1)
      setStorageKey(KEY_MEASURMENT_UNIT, UNIT_ENGLISH)
    }
    else {
      setSelectedIndex(0)
      setStorageKey(KEY_MEASURMENT_UNIT, UNIT_METIRC)
    }
    //MAP TYPE
    const tempMap =
      await getStorageKey(KEY_MAP_TYPE)
    if (tempMap === null || tempMap === "") {
      
      const isAppleMap= await getStorageKey(KEY_IS_APPLE_MAP_TYPE_SELECTED) 
      console.log("$$$"+isAppleMap)
      if (isAppleMap === "true") {
        setMaptype(MAP_STANDARD_IOS)
      } else {
        setMaptype(MAP_STANDARD)
      }
      setStorageKey(KEY_MAP_TYPE, MAP_STANDARD)

    } else {

      if (getPlatform() === "IOS") {
        if (tempMap === MAP_HYBRID) 
          setMaptype(MAP_HYBRID_IOS)
         else if (tempMap === MAP_STANDARD) 
         setMaptype(MAP_STANDARD_IOS)
         else if (tempMap === MAP_SATELLITE) 
         setMaptype(MAP_HYBRID_IOS)
      }else
        setMaptype(tempMap)
    }


    //Language Type
    const tempLanType = await getStorageKey(KEY_LANGUAGE_TYPE)
    if (tempLanType === null || tempLanType === "") {
      setStorageKey(KEY_LANGUAGE_TYPE, language_type)
      setLanguageType(LANGUAGE_CODE_ENGLISH) //device defualt language 
    }
    else
      setLanguageType(tempLanType)

    console.log("Map: " + tempMap + " Lan: " + tempLanType)

  }

  const callSettingApi = () => {
    setIsLoading(true)
    getSettingData()
  }

  const callSetLanguangeApi = () => {
    setIsLoading(true)
    sendLanguageData()
  }

  const clear = () => {
    clearAllData();
  };

  const logoutAlert = () =>
    Alert.alert(
      "Logout",
      "Do you want to logout ?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => clear() }
      ]
    );

  const handleSingleIndexSelect = (index) => {
    // For single Tab Selection SegmentedControlTab
    setSelectedIndex(index);
    if (index === 0)
      setStorageKey(KEY_MEASURMENT_UNIT, UNIT_MATIRC)
    else
      setStorageKey(KEY_MEASURMENT_UNIT, UNIT_ENGLISH)
  };

  return (
    <NativeBaseProvider >
      <View >
        <StatusBar translucent barStyle="light-content" />
      </View>

      <View style={{ marginTop: StatusBar.currentHeight }}>
        <CustomHeader
          title={SETTING_TITLE}
          navigation={navigation} />
      </View>
      <View style={styles.bg}>
        <Stack>
          <VStack space={5} alignItems="flex-start" margin="3">

            <HStack alignItems="center" space={4} >
              <Text style={styles.lable}>Capture Pole Id</Text>
              <Switch size="lg" alignContent="flex-end" marginRight="4"
                offThumbColor="#FFFF"
                onThumbColor="#FFF"
                onTrackColor="#50B848"
                isChecked={isPoleIdChecked}

                isDisabled="false"
              />
              {/* onValueChange={setIsPoleIdChecked(isPoleIdChecked)}
                value={isPoleIdChecked} */}
            </HStack>

            <HStack alignItems="center" space={4}>
              <Text style={styles.lable}>Measurement Units</Text>
              <SegmentedControlTab

                values={[UNIT_METIRC, UNIT_ENGLISH]}
                selectedIndex={selectedIndex}
                tabStyle={styles.tabStyle}
                activeTabStyle={styles.activeTabStyle}
                onTabPress={handleSingleIndexSelect}
              //onTabPress={setSelectedIndex(selectedIndex)}
              // onTabPress={setSelectedIndex(selectedIndex)}
              />
            </HStack>

            <HStack alignItems="center" space={4}>
              <Text style={styles.lable}>Pole Details</Text>
              <Switch size="lg" alignContent="flex-end" marginRight="5"
                offThumbColor="#FFFF"
                onThumbColor="#FFF"
                onTrackColor="#50B848"
                isChecked={isAssetView}
                isDisabled="false" />
            </HStack>

            <View style={styles.Seperator} />

            <HStack alignItems="center" space={4}>
              <Text style={styles.lable}>Language:</Text>
            </HStack>

            <HStack alignItems="center" >

              <Select
                flex="1"
                mx={5} my={2}
                selectedValue={language_type}
                fontSize={15}
                _selectedItem={{
                  bg: "gray",
                  endIcon: <CheckIcon size={5} />,
                }}
                mt="1"
                color="#ffff"
                onValueChange={(itemValue) => {
                  setLanguageType(itemValue)
                  setStorageKey(KEY_LANGUAGE_TYPE, itemValue)

                  //Api calling
                  callSetLanguangeApi()
                }
                }>
                <Select.Item label="English" value={LANGUAGE_CODE_ENGLISH} />
                <Select.Item label="Spanish" value={LANGUAGE_CODE_SPANISH} />
                <Select.Item label="Portugues" value={LANGUAGE_CODE_PORTUGUES} />
              </Select>

            </HStack>

            <HStack alignItems="center" space={1}>
              <Text style={styles.lable}>Map Types:</Text>

            </HStack>

            <HStack alignItems="center">

              {
                getPlatform() === 'IOS' ? (
                  <Select
                    flex="1"
                    mx={5} my={2}
                    selectedValue={maptype}
                    fontSize={15}
                    _selectedItem={{
                      bg: "gray",
                      endIcon: <CheckIcon size={5} />,
                    }}
                    mt="1"
                    color="#ffff"
                    onValueChange={(itemValue) => {
                      setMaptype(itemValue)

                      if (itemValue.includes("_ios")) {
                        const finalItem = itemValue.split("_")
                        console.log("Map Type ios: " + finalItem[0])
                        setStorageKey(KEY_MAP_TYPE, finalItem[0])
                        setStorageKey(KEY_IS_APPLE_MAP_TYPE_SELECTED, "true")
                      } else {
                        setStorageKey(KEY_MAP_TYPE, itemValue)
                        setStorageKey(KEY_IS_APPLE_MAP_TYPE_SELECTED, "false")
                      }
                    }}
                  >
                    <Select.Item label="Apple Standard Map" value={MAP_STANDARD_IOS} />
                    <Select.Item label="Apple Satellite Map" value={MAP_SATELLITE_IOS} />
                    <Select.Item label="Apple Hybrid Map" value={MAP_HYBRID_IOS} />
                    <Select.Item label="Google Standard Map" value={MAP_STANDARD} />
                    <Select.Item label="Google Satellite Map" value={MAP_SATELLITE} />
                    <Select.Item label="Google Hybrid Map" value={MAP_HYBRID} />

                  </Select>
                ) : (
                  <Select
                    flex="1"
                    mx={5} my={2}
                    selectedValue={maptype}
                    fontSize={15}
                    _selectedItem={{
                      bg: "gray",
                      endIcon: <CheckIcon size={5} />,
                    }}
                    mt="1"
                    color="#ffff"
                    onValueChange={(itemValue) => {
                      setMaptype(itemValue)
                      console.log("$$$" + itemValue)
                      setStorageKey(KEY_MAP_TYPE, itemValue)
                    }}
                  >
                    <Select.Item label="Google Standard Map" value={MAP_STANDARD} />
                    <Select.Item label="Google Satellite Map" value={MAP_SATELLITE} />
                    <Select.Item label="Google Hybrid Map" value={MAP_HYBRID} />
                  </Select>
                )
              }
            </HStack>

          </VStack>

        </Stack>

        <VStack space={1} alignItems="center">
          <Button
            style={styles.button}
            size="lg"

            onPress={logoutAlert}
            paddingLeft="10"
            paddingRight="10"

            _text={{
              color: "#1F2937",
            }}
          >Logout</Button>
          <Text
            style={styles.versionText}
          >Version:{version}</Text>

        </VStack>

      </View>
      <ProgressDialog visible={isLoading} />
    </NativeBaseProvider>
  );

}

const styles = StyleSheet.create({
  componant: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#0072BC",

  },
  bg: {
    backgroundColor: "#0072BC",
    flex: 1,

  },
  button: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 15,
    marginBottom: 5,

  },
  versionText: {
    color: "#ffffff",
    marginTop: 5,
    justifyContent: "center",
    alignItems: "center",

  },
  lable: {
    color: "#ffffff",
    fontSize: 16,
    flex: 1
  },
  Seperator: {
    marginHorizontal: -10,
    alignSelf: 'stretch',
    borderTopWidth: 1,
    borderTopColor: '#ffffff',
    marginTop: 24,
  },
  tabStyle: {
    flex: 0,
    borderColor: '#50B848',
    paddingHorizontal: 20
  },
  activeTabStyle: {
    backgroundColor: '#50B848',
  },
  tabTextStyle: {
    color: '#50B848',
  },

});

Setting.prototype = {
  getSettingData: propTypes.func.isRequired,
  responseState: propTypes.object.isRequired,
  sendLanguageData: propTypes.func.isRequired,
  navigtionState:propTypes.object.isRequired
};

const mapDispatchToProps = {
  getSettingData: (data) => getSettingData(data),
  sendLanguageData: (data) => sendLanguageData(data)
};

const mapStateToProps = (state) => ({
  responseState: state.SettingReducers,
  navigtionState: state.NavigationReducers,
  responseLanguageState: state.LanguageReducers
});

export default connect(mapStateToProps, mapDispatchToProps)(Setting);