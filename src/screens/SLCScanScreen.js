import { NativeBaseProvider, ZStack, Input, Center, FormControl, } from 'native-base'
import React, { Component, Fragment } from 'react'

import { Text, StatusBar, StyleSheet, View } from 'react-native'

import CustomHeader from '../layout/CustomHeader'
import { KEY_CUSTOMER_NAME, SLC_SCAN_TITLE } from '../utility/Constant'

import QRCodeScanner from 'react-native-qrcode-scanner';
import { getStorageKey, getClientDetails } from '../utility/Utility'
import AddSLCDialog from '../components/AddSLCDialog'
import { MOVE_TO_POLE_LOCATION_SCREEN } from '../action/action.types'
import ProgressDialog from '../components/ProgressDialog'
import { connect } from 'react-redux'


class SLCScanScreen extends Component {

    constructor(props) {
        super(props)

        const { item, otherParam } = this.props.route.params

        console.log("Mac address ------------------->" + item.mac_address);
        this.state = {
            scanResult: false,
            result: null,
            reactivate: true,
            showModal: false,
            inputValue: '',
            subTitle: '',
            isLoading: false,
            macAddress: item.mac_address,
        }
    }

    componentDidMount() {
        try {
            getCustomerName().then((value) => {
                this.setState({
                    customerName: value,
                })
            })
            randeringTitle().then((value) => {
                this.setState({ subTitle: `Enter ${value}` })
            })
        } catch (error) {
            console.log(error);
        }
    }

    onSuccessScan = (result) => {
        const check = result.data
        console.log("Scan QR code and result is " + check);
        this.setShowModal(true)
        this.setInputValue(check)
    }

    setShowModal = (isshow) => {

        if (!isshow) {
            this.setState({ reactivate: true })
            this.scanner.reactivate()
        } else {
            this.setState({ reactivate: false })
        }

        this.setState({
            showModal: isshow,
        })
    }


    setIsProgressBar = (isShow) => {
        this.setState({ isLoading: isShow })
    }

    setInputValue = (text) => {
        this.setState({
            inputValue: text,
        })
    }

    render() {
        const { reactivate, customerName, showModal, inputValue, macAddress, isLoading, subTitle } = this.state
        const { screen, data } = this.props.slcValidStatus
        const { navigation } = this.props
        console.log("=====================screen====================" + screen);

        if (screen != "") {

            if (screen == MOVE_TO_POLE_LOCATION_SCREEN) {

                this.setIsProgressBar(false)
                this.setShowModal(false)
                alert(MOVE_TO_POLE_LOCATION_SCREEN)
                console.log(MOVE_TO_POLE_LOCATION_SCREEN);
            }
            this.props.slcValidStatus.screen = ''
            this.props.slcValidStatus.data = ''
        }
        const ShowModel = () => {
            this.setShowModal(true);
        }

        const closeModel = () => {
            console.log("Close model");
            this.setShowModal(false)
        }

        const showProgressBar = () => {
            this.setIsProgressBar(true);
        }

        const closeProgressBar = () => {
            this.setIsProgressBar(false)
        }

        return (

            <NativeBaseProvider>
                <View >
                    <StatusBar translucent barStyle="light-content" />
                </View>
                <View style={{ marginTop: StatusBar.currentHeight }}>

                    <CustomHeader
                        title={SLC_SCAN_TITLE}
                        subTitle={subTitle}
                        onPress={ShowModel}
                        navigation={navigation} />

                </View>

                <ZStack style={styles.scrollViewStyle}>
                    <Fragment>
                        <QRCodeScanner
                            reactivate={reactivate}
                            showMarker={true}
                            ref={(node) => { this.scanner = node }}
                            onRead={this.onSuccessScan}
                            containerStyle={{ flex: 1, height: "100%" }}
                            cameraStyle={{
                                flex: 0, height: "100%",
                            }}
                        />
                    </Fragment>

                    <View style={styles.topView}>
                        <Text style={styles.text}>{customerName}</Text>
                    </View>

                    <View style={styles.bottomView}>
                        <Text style={styles.text}>Position code in box above</Text>
                    </View>

                    <Center>
                    </Center>
                </ZStack>

                <AddSLCDialog isDialogShow={showModal} slcValue={inputValue} onPress={closeModel}
                    macValue={macAddress} showProgress={showProgressBar} closeProgress={closeProgressBar} />
                <ProgressDialog visible={isLoading} />
            </NativeBaseProvider>
        )
    }
}

const getCustomerName = async () => {
    let customerName = await getStorageKey(KEY_CUSTOMER_NAME)
    return customerName;
}

const randeringTitle = async () => {
    const { ScanLBL } = await getClientDetails()
    console.log('print title : ' + ScanLBL);
    return ScanLBL;
}

const styles = StyleSheet.create({
    componant: {
        flex: 1,
        alignItems: "center",
        justifyContent: 'center',
    },
    scrollViewStyle: {
        flex: 1,
    },
    topView: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        width: "100%",
        alignItems: "center",
        padding: 10,
    },
    text: {
        fontSize: 16,
        fontStyle: 'normal',
        fontWeight: '500',
        color: 'white',
    },
    bottomView: {
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        width: "100%",
        alignItems: "center",
        padding: 10,
        bottom: 0,
    }

})

const mapStateToProps = state => ({
    slcValidStatus: state.AddSLCReducers,
})

export default connect(mapStateToProps)(SLCScanScreen);
