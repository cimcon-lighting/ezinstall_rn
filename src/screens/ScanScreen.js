import { NativeBaseProvider, ZStack, Input, Center } from 'native-base'
import React, { Component, Fragment } from 'react'

import { Text, StatusBar, StyleSheet, View } from 'react-native'

import CustomHeader from '../layout/CustomHeader'
import { KEY_CUSTOMER_NAME, MAC_SCAN_TITLE } from '../utility/Constant'

import QRCodeScanner from 'react-native-qrcode-scanner';
import { getStorageKey, getClientDetails } from '../utility/Utility'
import AddMacDialog from '../components/AddMacDialog'
import { connect } from 'react-redux'

import { MOVE_TO_SLC_SCREEN } from '../action/action.types'
import ProgressDialog from '../components/ProgressDialog'


class Scan extends Component {

    constructor(props) {
        super(props)
        this.state = {
            scanResult: false,
            result: null,
            reactivate: true,
            showModal: false,
            inputValue: '',
            subTitle: '',
            isLoading: false
        }
    }

    componentDidMount() {
        try {
            getCustomerName().then((value) => {
                this.setState({
                    customerName: value,
                })
            })
            randeringTitle().then((value) => {
                this.setState({ subTitle: `Enter ${value}` })
            })
        } catch (error) {
            console.log(error);
        }
    }

    onSuccessScan = (result) => {
        const check = result.data
        console.log("Scan QR code and result is " + check);
        this.setShowModal(true)
        this.setInputValue(check)
    }

    setShowModal = (isshow) => {

        if (!isshow) {
            this.setState({ reactivate: true })
            this.scanner.reactivate()
        } else {
            this.setState({ reactivate: false })
        }

        this.setState({
            showModal: isshow,
        })
    }

    setIsProgressBar = (isShow) => {
        this.setState({ isLoading: isShow })
    }

    setInputValue = (text) => {
        this.setState({
            inputValue: text,
        })
    }

    render() {

        const { navigation } = this.props

        const { reactivate, customerName, showModal, inputValue, subTitle, isLoading } = this.state
        const { screen, data } = this.props.macValidStatus

        console.log("=====================ISLOADING====================" + screen);

        console.log("=====================ISLOADING====================" + data.mac_address);

        if (screen != "") {
            this.setIsProgressBar(false)
            this.setShowModal(false)

            this.props.macValidStatus.screen = ''
            this.props.macValidStatus.data = ''

            if (screen == MOVE_TO_SLC_SCREEN) {

                this.props.navigation.navigate('SLC Scan', {
                    item: data,
                    otherParam: 'anything you want here',
                })
            }
        }

        const ShowModel = () => {
            this.setShowModal(true);
        }

        const closeModel = () => {
            console.log("Close model");
            this.setShowModal(false)
        }

        const showProgressBar = () => {
            this.setIsProgressBar(true);
        }

        const closeProgressBar = () => {
            this.setIsProgressBar(false)
        }

        return (

            <NativeBaseProvider>
                <View >
                    <StatusBar translucent barStyle="light-content" />
                </View>
                <View style={{ marginTop: StatusBar.currentHeight }}>

                    <CustomHeader
                        title={MAC_SCAN_TITLE}
                        subTitle={subTitle}
                        onPress={ShowModel}
                        navigation={navigation} />
                </View>

                <ZStack style={styles.scrollViewStyle}>
                    <Fragment>
                        <QRCodeScanner
                            reactivate={reactivate}
                            showMarker={true}
                            ref={(node) => { this.scanner = node }}
                            onRead={this.onSuccessScan}
                            containerStyle={{ flex: 1, height: "100%" }}
                            cameraStyle={{
                                flex: 0, height: "100%",
                            }}
                        />
                    </Fragment>

                    <View style={styles.topView}>
                        <Text style={styles.text}>{customerName}</Text>
                    </View>

                    <View style={styles.bottomView} >
                        <Text style={styles.text}>Position code in box above</Text>
                    </View>

                    <Center>
                    </Center>
                </ZStack>

                <AddMacDialog isDialogShow={showModal} macValue={inputValue} subTitle={subTitle}
                    onPress={closeModel} showProgress={showProgressBar} closeProgress={closeProgressBar} />
                <ProgressDialog visible={isLoading} />
            </NativeBaseProvider>
        )
    }
}

const getCustomerName = async () => {
    let customerName = await getStorageKey(KEY_CUSTOMER_NAME)
    return customerName;
}

const randeringTitle = async () => {
    const { ScanLBL } = await getClientDetails()
    console.log('print title : ' + ScanLBL);
    return ScanLBL;
}

const styles = StyleSheet.create({
    componant: {
        flex: 1,
        alignItems: "center",
        justifyContent: 'center',
    },
    scrollViewStyle: {
        flex: 1,
    },
    topView: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        width: "100%",
        alignItems: "center",
        padding: 10,
    },
    text: {
        fontSize: 16,
        fontStyle: 'normal',
        fontWeight: '500',
        color: 'white',
    },
    bottomView: {
        backgroundColor: 'rgba(52, 52, 52, 0.5)',
        width: "100%",
        alignItems: "center",
        padding: 10,
        bottom: 0,
    }

})

const mapStateToProps = state => ({
    macValidStatus: state.AddMacReducers,
})

export default connect(mapStateToProps)(Scan);