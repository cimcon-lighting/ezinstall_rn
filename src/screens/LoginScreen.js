import React, { useState, useEffect } from 'react'

import { BIOMETRIC_SUCCESS, BIOMETRIC_FAIED, LOGIN_SUCCESS_NAV, SECURITY_SUCCESS_NAV } from '../action/action.types'

import {
    TouchableOpacity,
    StyleSheet,
    SafeAreaView,
    StatusBar,
    View
} from 'react-native'

import {
    FormControl,
    Input,
    VStack,
    HStack,
    Checkbox,
    Button,
    Image, NativeBaseProvider, Box, Center
} from "native-base";

import { connect, useDispatch } from 'react-redux'
import propTypes from 'prop-types'
import Icon from 'react-native-vector-icons/Ionicons';
import { doLogin } from '../middleware/LoginMiddleware'
import { getLocation, hasLocationPermission } from '../utility/GeoLocationUtility'
import ProgressDialog from '../components/ProgressDialog'
import { KEY_USERNAME, KEY_PASSWORD, KEY_IS_REMEMBER, IS_LOGGED_IN, IS_SECURITY_CODE_EXIST, KEY_CUSTOMER_NAME, KEY_LOGIN_TOKEN } from '../utility/Constant'
import { setStorageKey, getStorageKey } from '../utility/Utility'
import { isBiometricSupported, invokeBiometric } from '../utility/BiometricUtility';

const LoginScreen = ({ invokeBiometric, isBiometricSupported, doLogin, navigation, responseState, callbackBio }) => {

    const [username, setUsername] = useState('darshan.barodia@cimconlighting.com')
    const [password, setPassword] = useState('cimcon@123')
    // const [username, setUsername] = useState('')
    // const [password, setPassword] = useState('')
    const [checked, setChecked] = useState(false);
    const [passwordVisibleIcon, setPasswordVisibleIcon] = useState('eye-off');
    const [isPasswordVisible, setIsPasswordVisible] = useState(true);
    const [isValidUsername, setIsValidUsername] = useState(false)
    const [isValidPassword, setIsValidPassword] = useState(false)
    const [isLocationPermission, setLocationPermission] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [isBioSupport, setIsBioSupport] = useState(false)

    const dispatch = useDispatch();

    let storePassword = ''

    const setRememberState = (isManual) => {
        if (checked) {
            console.log('Remember check true');
            setStorageKey(KEY_IS_REMEMBER, 'true')
            setStorageKey(KEY_USERNAME, username)
            if (storePassword != '' && password == '') {
                setStorageKey(KEY_PASSWORD, storePassword)
            } else {
                setStorageKey(KEY_PASSWORD, password)
            }
        } else {

            if (!isManual) {
                console.log('Remember check true, Reason : Login with FigurePrint');
                setChecked(true)
                setStorageKey(KEY_IS_REMEMBER, 'true')
                setStorageKey(KEY_USERNAME, username)
                setStorageKey(KEY_PASSWORD, password)
            } else {
                console.log('Remember check false');
                setStorageKey(KEY_IS_REMEMBER, 'false')
                setStorageKey(KEY_USERNAME, '')
                setStorageKey(KEY_PASSWORD, '')
            }
        }
    }

    const getRememberState = async () => {
        let isRemember = await getStorageKey(KEY_IS_REMEMBER)
        console.log('getRememberState : ' + isRemember);
        if (isRemember != '' && isRemember == 'true') {
            setChecked(true)
            let userName = await getStorageKey(KEY_USERNAME)
            setUsername(userName)
        } else {
            setChecked(false)
        }
    }

    useEffect(() => {

        setIsLoading(false)
        if (!responseState.loading) {
            const data = responseState.data.payload
            console.log(`LoginScreen : ${data.msg}`);
            if (data.status == '-1' || data.status == 'error') {
                alert(data.msg)
            } else {
                // navigation.navigate("Tabbar")
                setStorageKey(IS_LOGGED_IN, 'true')
                setStorageKey(KEY_CUSTOMER_NAME, data.customer_name)
                setStorageKey(KEY_LOGIN_TOKEN, data.token);
                redirectDashBoard()
            }
        }
    }, [responseState])

    useEffect(() => {

        if (callbackBio.supported == 'true') {
            setIsBioSupport(true)
        } else if (callbackBio.supported == 'false') {
            setIsBioSupport(false)
        }

        if (callbackBio.data == BIOMETRIC_SUCCESS) {
            console.log("Callback of response :" + callbackBio.data)
            doLoginApiCall('', '', false)
        }

    }, [callbackBio])

    useEffect(() => {

        const permissioStatus = hasLocationPermission()

        if (permissioStatus) {
            setLocationPermission(true)
        } else {
            setLocationPermission(false)
        }

        if (isLocationPermission) {
            console.log("Capture location");
            getLocation()
        } else {
            console.log("location service disable");
            hasLocationPermission().then((isGranted) => {
                console.log("location service is " + isGranted);
                setLocationPermission(true)
                getLocation()
            });
        }

        getRememberState()
        isBiometricSupported()
    }, [])

    const changePasswordVisibleState = () => {

        if (isPasswordVisible) {
            setIsPasswordVisible(false)
            setPasswordVisibleIcon('eye')
        } else {
            setIsPasswordVisible(true)
            setPasswordVisibleIcon('eye-off')
        }
    }

    const validation = async (isManual) => {

        let isValid = true

        if (username == '') {
            setIsValidUsername(true);
            isValid = false
        } else {
            setIsValidUsername(false);
        }

        if (isManual) {
            if (password == '') {
                setIsValidPassword(true);
                isValid = false
            } else {
                setIsValidPassword(false);
            }
        } else {

            if (password != '') {
                await setStorageKey(KEY_PASSWORD, password)
            }

            const sPassword = await getStorageKey(KEY_PASSWORD)
            console.log("Password value : " + sPassword);
            if (sPassword == '') {
                setIsValidPassword(true);
                isValid = false
            } else {
                storePassword = sPassword
                setIsValidPassword(false);
            }
        }
        console.log("isLoginValidation : " + isValid);
        return isValid;
    }

    const doFigurePrintClick = async () => {
        console.log("User click to login with figureprint");
        const isValid = await validation(false);
        console.log("validation : " + isValid);
        if (isValid) {
            setRememberState(false)
            console.log("User enter valid credentials");
            invokeBiometric()
        } else {
            console.log("User enter credentials is empty");
        }
    }

    const doLoginCall = async () => {
        console.log("User click to login with manual");
        const isValid = await validation(true)

        console.log("validation : " + isValid);
        if (isValid) {
            setRememberState(true)
            console.log("User enter valid credentials");
            doLoginApiCall(username, password, true)
        } else {
            console.log("User enter credentials is empty");
        }
    }

    const doLoginApiCall = async (loginUsername, loginPassword, isManual) => {
        setIsLoading(true)
        if (!isManual) {
            loginPassword = await getStorageKey(KEY_PASSWORD)
            loginUsername = username
        }
        doLogin({ username: loginUsername, pass: loginPassword })
    }

    const redirectDashBoard = () => {

        console.log("redirect login");
        const data = {
            type: LOGIN_SUCCESS_NAV,
            payload: true
        }
        dispatch(data)
    }

    const goBackScreen = () => {

        console.log("redirect Security Screen");
        setStorageKey(IS_SECURITY_CODE_EXIST, 'false')
        const data = {
            type: SECURITY_SUCCESS_NAV,
            payload: false
        }

        dispatch(data)
    }


    return (
        <NativeBaseProvider>
            <Box>
                <View>
                    <StatusBar translucent barStyle="dark-content" />
                </View>
                <VStack space={5}>
                    <TouchableOpacity style={{ flex: 0, paddingTop: StatusBar.currentHeight + 36, paddingLeft: 5 }}
                        onPress={() => goBackScreen()}>

                        <Icon name="chevron-back-sharp" size={35} color="#0072BC" />
                    </TouchableOpacity>

                    <Center flex="1" mt={20}>
                        <Image source={require('../assets/img/logo.jpg')}
                            w={150} h={150} alt="No Image" />
                    </Center>

                    <VStack space={4} mt={20} ml={10} mr={10}>
                        <FormControl
                            isInvalid={
                                isValidUsername
                            }
                        >
                            <Input pl={5}
                                pr={5}
                                pt={4}
                                pb={4}
                                size="2xl"
                                returnKeyType={"next"}
                                keyboardType="email-address"
                                value={username}
                                onChangeText={(text) => setUsername(text)}
                                variant="outline" placeholder='Username' />
                            <FormControl.ErrorMessage>
                                Please enter username
                            </FormControl.ErrorMessage>
                        </FormControl>
                        <FormControl
                            isInvalid={
                                isValidPassword
                            }
                        >
                            <Input
                                pl={5}
                                pr={5}
                                pt={4}
                                pb={4}
                                InputRightElement={
                                    <Icon

                                        onPress={changePasswordVisibleState}
                                        style={{ paddingLeft: 10, paddingRight: 10 }} name={passwordVisibleIcon} size={25} color="#999999" />
                                }
                                size="2xl"
                                secureTextEntry={isPasswordVisible}
                                value={password}
                                onChangeText={(text) => setPassword(text)}
                                type="password" variant="outline" placeholder='Password' />

                            <FormControl.ErrorMessage>
                                Please enter password
                            </FormControl.ErrorMessage>
                        </FormControl>

                        <HStack style={{
                            alignItems: "center"
                        }}>
                            <Checkbox
                                isChecked={checked}
                                colorScheme="green"
                                onPress={() => {
                                    setChecked(!checked)
                                }}
                            >Remember me</Checkbox>

                        </HStack>
                    </VStack>
                    <VStack space={3}>
                        <Button
                            _text={{
                                fontSize: 'lg'
                            }}
                            ml={35}
                            mr={35}
                            mt={5}
                            onPress={() => doLoginCall()}
                            mode="contained"
                            size="lg"
                            colorScheme="green" >
                            LOGIN
                        </Button>

                        {
                            isBioSupport ? (
                                <Button
                                    leftIcon={<Icon name="finger-print-sharp" color='white' size={25} />}
                                    _text={{
                                        fontSize: 'lg'
                                    }}
                                    ml={35}
                                    mr={35}
                                    onPress={() => doFigurePrintClick()}
                                    mode="contained"
                                    size="lg"
                                    colorScheme="green" >
                                    Login with Touch ID
                                </Button>
                            ) : (<></>)
                        }
                    </VStack>
                </VStack>
            </Box>
            <ProgressDialog visible={isLoading} />
        </NativeBaseProvider>
    )
}

LoginScreen.prototype = {
    doLogin: propTypes.func.isRequired,
    responseState: propTypes.object.isRequired,
    callbackBio: propTypes.object.isRequired,
}

const mapDispatchToProps = {
    doLogin: (data) => doLogin(data),
    isBiometricSupported: () => isBiometricSupported(),
    invokeBiometric: () => invokeBiometric(),
}

const mapStateToProps = (state) => ({
    responseState: state.LoginReducers,
    callbackBio: state.BiometricReducers,
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)

const styles = StyleSheet.create({
    button: {
        marginStart: 30,
        marginEnd: 30,
        marginTop: 10,
    },
    btnText: {
        color: 'white',
        fontSize: 30,
    },
    container: {
        flex: 1,
    },
})