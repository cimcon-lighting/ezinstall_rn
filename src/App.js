/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React, { useEffect } from "react";
import { Text } from "react-native";
import LoginScreen from "./screens/LoginScreen";
import Tabbar from "./screens/Tabbar";
import MapScreen from './screens/MapScreen'
import SLCDetailsViewScreen from './screens/SLCDetailsViewScreen'

import SecurityCodeScreen from "./screens/SecurityCodeScreen";
import SplashScreen from "react-native-splash-screen";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import SplashscreenJS from './screens/SplashScreen'

import { connect, useDispatch } from 'react-redux'
import propTypes from 'prop-types'
import { getStorageKey } from "./utility/Utility";
import { IS_LOGGED_IN, IS_SECURITY_CODE_EXIST } from "./utility/Constant";
import { LOGIN_SUCCESS_NAV, SECURITY_SUCCESS_NAV } from "./action/action.types";
import EmptyContainer from "./components/EmptyContainer";

const Stack = createStackNavigator();

const App = ({ screenState }) => {

  const dispatch = useDispatch();

  useEffect(() => {
    redirectScreen();
    SplashScreen.hide();
  }, []);


  const redirectScreen = async () => {

    const isLogin = await getStorageKey(IS_LOGGED_IN)

    if (isLogin) {

      console.log("redirect dashboard screen");
      const data = {
        type: LOGIN_SUCCESS_NAV,
        payload: true
      }
      dispatch(data)

    } else {

      const isSecurityCodeExits = await getStorageKey(IS_SECURITY_CODE_EXIST)

      if (isSecurityCodeExits) {
        console.log("redirect login");
        const data = {
          type: SECURITY_SUCCESS_NAV,
          payload: true
        }

        dispatch(data)
      } else {
        console.log("redirect Security Screen");
        const data = {
          type: SECURITY_SUCCESS_NAV,
          payload: false
        }

        dispatch(data)
      }
    }
  }

  if (screenState.isLoading) {
    return <SplashscreenJS />
    //return <EmptyContainer />
  }

  {
    console.log("screenState.isSecuritySuccess ----> " + screenState.isSecuritySuccess);
    console.log("screenState.isLoginSuccess ----> " + screenState.isLoginSuccess);
    
  }

  return (
    <>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='SecurityCodeScreen'>

          {screenState.isLoginSuccess ? (
            <>
              <Stack.Screen name="Tabbar" component={Tabbar} options={{ headerShown: false }} />
            </>
          ) : (
            <>
              {
                screenState.isSecuritySuccess ? (
                  <Stack.Screen name="LoginScreen" component={LoginScreen} options={{ headerShown: false }} />
                ) : (
                  <Stack.Screen name="SecurityCodeScreen" component={SecurityCodeScreen} options={{ headerShown: false }} />
                )}
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

App.prototype = {
  screenState: propTypes.object.isRequired,
}

const mapStateToProps = (state) => ({
  screenState: state.NavigationReducers,
})

export default connect(mapStateToProps)(App);