import React, { useEffect, Component } from 'react'

import { Appbar } from 'react-native-paper'
import { View } from 'react-native'

import {
  StyleSheet
} from 'react-native'

import {
  SLC_LIST_TITLE,
  SLC_DETAILS_TITLE,
  MAC_SCAN_TITLE,
  MAP_TITLE,
  SLC_SCAN_TITLE,
  SETTING_TITLE
} from '../utility/Constant'

import { getClientDetails } from '../utility/Utility'


class CustomHeader extends Component {

  constructor(props) {
    super(props)
    this.state = {
      subTitle: ''
    }
  }

  componentDidMount() {
    if (this.props.title == MAC_SCAN_TITLE) {

      randeringTitle().then((value) => {

        console.log("value - " + value);
        this.setState({
          subTitle: value
        })
      })
    }
  }

  render() {
    const { subTitle } = this.state
    const { title, onPress, navigation } = this.props

    console.log("TITLE : " + title)

    const renderingOptions = () => {
      if (title === SLC_LIST_TITLE) {
        return (
          <Appbar.Action icon="magnify" size={20} color="white" onPress={onPress} />
        )
      } else if (title === SLC_DETAILS_TITLE) {
        return (
          <Appbar.Action icon="pin" size={20} color="white" onPress={onPress} />
        )
      } else if (title === MAC_SCAN_TITLE || title === SLC_SCAN_TITLE) {
        console.log("title scan show icon");
        return (
          <Appbar.Action icon="cursor-pointer" size={20} color="white" onPress={onPress} />
        )
      }
    }

    return (
      <Appbar.Header
        style={{ backgroundColor: "#01317B" }} >
        {
          (title !== SLC_LIST_TITLE &&
            title !== MAC_SCAN_TITLE &&
            title !== MAP_TITLE &&
            title !== SETTING_TITLE
            ) && (
            <Appbar.BackAction color="white" size={20} onPress={() => { navigation.goBack() }} />
          )
        }
        <View
          style={[
            StyleSheet.absoluteFill,
            { alignItems: "center", justifyContent: "center" },
          ]}
          pointerEvents="box-none"
        >
          {
            title == MAC_SCAN_TITLE ? (
              <Appbar.Content
                title={title + " " + subTitle}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              />
            ) : (
              <Appbar.Content
                title={title}
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                }}
              />
            )
          }
        </View>
        <View style={{ flex: 1 }} />
        {
          renderingOptions()
        }
      </Appbar.Header>
    )
  }
}
const randeringTitle = async () => {
  const { ScanLBL } = await getClientDetails()
  return ScanLBL;
}
export default CustomHeader;

const styles = StyleSheet.create({
  container: {
    flex: 0,
    backgroundColor: '#1b262c',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  navigationContainer: {
    flex: 1,
  },
})