import { API_SECURITY_CODE, POST } from "../utility/config";
import { networkManager } from "../action/NetworkManager";
import {
  SCODE_SUCCESS,
  SCODE_ERROR,
  API_SUCCESS,
  API_ERROR,
} from "../action/action.types";

import { getPlatform, getLanguage,getDeviceUniqueId } from "../utility/Utility";

export const doSecurity = (data) => async (dispatch) => {
  const { code } = data;

  //identify platform
  const source = getPlatform();

  //identify current language locale
  const language = getLanguage();

  //getUniqueId
  const uniqueId = getDeviceUniqueId();

  const requestObject = {
    code: code,
    udid: uniqueId,
    source: source,
    language: language,
  };

  const request = {
    url: API_SECURITY_CODE,
    method: POST,
    data: requestObject,
  };

  let response = await networkManager(request);
  if (response.type == API_SUCCESS) {
    const data = {
      type: SCODE_SUCCESS,
      payload: response,
    };
    dispatch(data);
  } else if (response.type == API_ERROR) {
    const data = {
      type: SCODE_ERROR,
      payload: response,
    };
    dispatch(data);
  }
};
/* Expected Output:
{
	"status": "success",
	"msg": "Valid Security Code",
	"ClientID": "261",
	"userid": "1033",
	"ScanLBL": "UID",
	"ScanPH": "UID",
	"data": {
		"ClientID": "261",
		"ScanLBL": "UID",
		"ScanPH": "UID",
		"userid": "1033"
	},
	"setting": {
		"client_slc_list_view": "Yes",
		"client_slc_edit_view": "Yes",
		"client_slc_pole_image_view": "Yes",
		"client_slc_pole_assets_view": "Yes",
		"client_slc_pole_id": "Yes"
	}
}
*/
