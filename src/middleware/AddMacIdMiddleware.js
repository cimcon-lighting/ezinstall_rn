
import { API_CHECK_INTERNAL_UNIQUE_MACADDRESS, API_CHECK_MAC_IN_LG, POST } from '../utility/config'
import { networkManager } from '../action/NetworkManager'
import { getPlatform, getClientDetails } from '../utility/Utility'
import { API_SUCCESS, API_ERROR, MOVE_TO_SLC_SCREEN, MOVE_TO_SCREEN_FROM_MAC_SCAN, MOVE_TO_LOGIN_SCREEN, MOVE_TO_ERROR_MAC, MOVE_TO_POLE_LOCATION_SCREEN, IS_CONTINUE_DIALOG } from '../action/action.types';


export const doCheckValidMacAddress = (data) => async (dispatch) => {
    const { macValue } = data
    const { ClientID, userid } = await getClientDetails()

    const requestObject = {
        client_id: ClientID,
        user_id: userid,
        mac_address: macValue,
        source: getPlatform(),
    }

    const request = {
        url: API_CHECK_INTERNAL_UNIQUE_MACADDRESS,
        method: POST,
        data: requestObject
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        console.log("API SUCCESS");
        let apiResponse = response.payload;

        switch (apiResponse.status.toUpperCase()) {

            case 'CONTINUE':
                // Ask yes/no permission to user
                console.log("status is continue");

                dispatch({
                    type: IS_CONTINUE_DIALOG,
                    payload: apiResponse
                })

                break;

            case 'SUCCESS':
                console.log("Status is success");
                validateMacAddressType({ value: macValue, response: apiResponse, dispatch: dispatch })

                break;

            default:
                console.log("Status is error");

                dispatch({
                    type: MOVE_TO_ERROR_MAC,
                    payload: apiResponse.msg
                })

                break;
        }
    } else if (response.type == API_ERROR) {
        console.log("API Error");
    }
}

export const validateMacAddressType = (data) => {
    const { value, response, dispatch } = data

    console.log("Inside - " + value);

    switch (response.mac_address_type.toUpperCase()) {

        case 'EXTERNAL':
            /* IF macType external then validate mac value in LG available or not
            * */
            console.log("Mac addrress is External, Check mac in LG");
            validateMacAddressInLG({ value, dispatch })
            break;

        case 'INTERNAL':
            // Going to Pole Location Fragment
            console.log("Mac address is internal");

            const data = {
                mac_address: value,
                slc_id: response.slc_id
            }

            dispatch({
                type: MOVE_TO_SCREEN_FROM_MAC_SCAN,
                payload: MOVE_TO_POLE_LOCATION_SCREEN,
                data: data
            })

            break;
    }
}

const validateMacAddressInLG = async (data) => {

    const { value, dispatch } = data
    const { ClientID, userid } = await getClientDetails()

    console.log("value - >" + value);

    const requestObject = {
        client_id: ClientID,
        user_id: userid,
        macaddress: value,
        source: getPlatform(),
    }

    const request = {
        url: API_CHECK_MAC_IN_LG,
        method: POST,
        data: requestObject
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        console.log("API SUCCESS");
        let apiResponse = response.payload;

        switch (apiResponse.status.toUpperCase()) {

            case 'SUCCESS':
                // Going to SLC Scan Fragment
                console.log("Going to SLC Scan Screen");

                const data = {
                    mac_address: value
                }

                dispatch({
                    type: MOVE_TO_SCREEN_FROM_MAC_SCAN,
                    payload: MOVE_TO_SLC_SCREEN,
                    data: data,
                })

                break;

            case 'LOGOUT':
                // Going to Login Screen
                console.log("Going to Login Screen");

                dispatch({
                    type: MOVE_TO_SCREEN_FROM_MAC_SCAN,
                    payload: MOVE_TO_LOGIN_SCREEN
                })
                break;


            case 'ERROR':
                console.log("Status is error");

                dispatch({
                    type: MOVE_TO_ERROR_MAC,
                    payload: apiResponse.msg
                })

                break;

            default:
                console.log("Status is error");

                dispatch({
                    type: MOVE_TO_ERROR_MAC,
                    payload: apiResponse.msg
                })

                break;
        }
    } else if (response.type == API_ERROR) {
        console.log("API Error");
        alert('Someting problem, Please try after sometime.')
    }

}
