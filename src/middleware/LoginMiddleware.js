
import { API_LOGIN, POST } from '../utility/config'
import { networkManager } from '../action/NetworkManager'
import { getLocation, getPlatform, getClientDetails } from '../utility/Utility'
import { API_PENDING, API_SUCCESS, API_ERROR } from '../action/action.types';
import { LOGIN_PENDING, LOGIN_SUCCESS, LOGIN_ERROR } from '../action/action.types';

export const doLogin = (data) => async (dispatch) => {
    const { username, pass } = data
    const { latitude, longitude } = await getLocation()
    const { ClientID, userid } = await getClientDetails()

    const requestObject = {
        client_id: ClientID,
        userid: userid,
        username: username,
        password: pass,
        lat: latitude,
        long: longitude,
        source: getPlatform(),
        Version: '5.3.4',
    }

    const request = {
        url: API_LOGIN,
        method: POST,
        data: requestObject
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        const data = {
            type: LOGIN_SUCCESS,
            payload: response
        }
        dispatch(data)
    } else if (response.type == API_ERROR) {
        const data = {
            type: LOGIN_ERROR,
            payload: response
        }
        dispatch(data)
    }
}
