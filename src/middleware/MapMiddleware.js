import { API_SLC_LIST_USING_LATLONG, POST } from "../utility/config";
import { networkManager } from "../action/NetworkManager";
import {
  SLC_LIST_ON_MAP_SUCCESS,
  SLC_LIST_ON_MAP_ERROR,
  API_SUCCESS,
  API_ERROR,
} from "../action/action.types";

export const doSLCLatLong = (data) => async (dispatch) => {
  const { latitude,longitude,kilometer,clientid,userid, top_lat,top_lon,bottom_lat,bottom_lon } = data;

  const requestObject = {
    latitude:latitude,
    longitude:longitude,
    kilometer:kilometer,
    clientid:clientid,
    userid:userid,
    top_lat:top_lat,
    top_lon:top_lon,
    bottom_lat:bottom_lat,
    bottom_lon:bottom_lon
  };

  const request = {
    url: API_SLC_LIST_USING_LATLONG,
    method: POST,
    data: requestObject,
  };

  let response = await networkManager(request);
  if (response.type == API_SUCCESS) {
    const data = {
      type: SLC_LIST_ON_MAP_SUCCESS,
      payload: response,
    };
    dispatch(data);
  } else if (response.type == API_ERROR) {
    const data = {
      type: SLC_LIST_ON_MAP_ERROR,
      payload: response,
    };
    dispatch(data);
  }
};