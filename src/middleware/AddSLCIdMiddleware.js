
import { 
    API_CHECK_SLCID, 
    API_CHECK_MAC_WITH_SLC, 
    POST 
} from '../utility/config'
import { networkManager } from '../action/NetworkManager'
import { getPlatform, getClientDetails } from '../utility/Utility'
import { 
    API_SUCCESS, 
    API_ERROR, 
    MOVE_TO_LOGIN_SCREEN, 
    MOVE_TO_SCREEN_FROM_SLC_SCAN, 
    MOVE_TO_ERROR_SLC, 
    IS_CONTINUE_DIALOG, 
    MOVE_TO_POLE_LOCATION_SCREEN 
} from '../action/action.types';

export const doCheckValidSLCId = (data) => async (dispatch) => {
    const { slcid, macAddress } = data
    const { ClientID, userid } = await getClientDetails()

    const requestObject = {
        client_id: ClientID,
        user_id: userid,
        slcid: slcid,
    }

    const request = {
        url: API_CHECK_SLCID,
        method: POST,
        data: requestObject
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        console.log("API SUCCESS");
        let apiResponse = response.payload;

        switch (apiResponse.status.toUpperCase()) {

            case 'SUCCESS':
                console.log("Status is success");
                console.log("Check SLC in LG");
                validateSLCIdInLG({ slcid: slcid, macid: macAddress, ClientID, userid, dispatch })

                break;

            case 'LOGOUT':
                // Going to Login Screen
                console.log("Going to Login Screen");

                dispatch({
                    type: MOVE_TO_SCREEN_FROM_SLC_SCAN,
                    payload: MOVE_TO_LOGIN_SCREEN
                })
                break;

            default:
                console.log("Status is error");

                dispatch({
                    type: MOVE_TO_ERROR_SLC,
                    payload: apiResponse.msg
                })

                break;
        }
    } else if (response.type == API_ERROR) {
        console.log("API Error");
    }
}

const validateSLCIdInLG = async (data) => {

    const { slcid, macid, ClientID, userid, dispatch } = data

    const requestObject = {
        client_id: ClientID,
        user_id: userid,
        mac_address: macid,
        slc_id: slcid,
        source: getPlatform(),
        node_type: '',
    }

    const request = {
        url: API_CHECK_MAC_WITH_SLC,
        method: POST,
        data: requestObject
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        console.log("API SUCCESS");
        let apiResponse = response.payload;

        switch (apiResponse.status.toUpperCase()) {

            case 'SUCCESS':
                // Going to SLC Scan Fragment
                console.log("SLC valid, move to location screen");

                dispatch({
                    type: MOVE_TO_SCREEN_FROM_SLC_SCAN,
                    payload: MOVE_TO_POLE_LOCATION_SCREEN
                })

                break;

            case 'CONTINUE':
                // Ask yes/no permission to user
                console.log("status is continue,  show alert to user");

                dispatch({
                    type: IS_CONTINUE_DIALOG,
                    payload: apiResponse
                })

                break;

            case 'ERROR':
                console.log("Status is error");

                dispatch({
                    type: MOVE_TO_ERROR_SLC,
                    payload: apiResponse.msg
                })

                break;

            default:
                console.log("Status is error");

                dispatch({
                    type: MOVE_TO_ERROR_SLC,
                    payload: apiResponse.msg
                })

                break;
        }
    } else if (response.type == API_ERROR) {
        console.log("API Error");
        alert('Someting problem, Please try after sometime.')
    }

}
