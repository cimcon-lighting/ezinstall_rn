import { API_CHANGE_LANGUAGE, API_SETTING, GET } from "../utility/config";
import { networkManager } from "../action/NetworkManager";
import {
  SETTING_SUCCESS,
  SETTING_ERROR,
  API_SUCCESS,
  API_ERROR,
  LANGUAGE_SUCCESS,
  LANGUAGE_ERROR
} from "../action/action.types";

import {getClientDetails, getStorageKey} from '../utility/Utility';
import { KEY_LANGUAGE_TYPE, KEY_MEASURMENT_UNIT } from "../utility/Constant";

export const getSettingData = () => async (dispatch) => {

  const { ClientID } = await getClientDetails()

  const request = {
    url: `${API_SETTING}/${ClientID}`,
    method: GET,
    data: {},
  };

  let response = await networkManager(request);
  if (response.type == API_SUCCESS) {
    const data = {
      type: SETTING_SUCCESS,
      payload: response,
    };
    dispatch(data);
  } else if (response.type == API_ERROR) {
    const data = {
      type: SETTING_ERROR,
      payload: response,
    };
    dispatch(data);
  }
};

export const sendLanguageData = () => async (dispatch) => {

  const { ClientID,userid } = await getClientDetails()
  const language=await getStorageKey(KEY_LANGUAGE_TYPE)

  const request = {
    url: `${API_CHANGE_LANGUAGE}/${userid}/${language}/${ClientID}`,
    method: GET,
    data: {},
  };

  let response = await networkManager(request);
  if (response.type == API_SUCCESS) {
    const data = {
      type: LANGUAGE_SUCCESS,
      payload: response,
    };
    dispatch(data);
  } else if (response.type == API_ERROR) {
    const data = {
      type: LANGUAGE_ERROR,
      payload: response,
    };
    dispatch(data);
  }
};

/* Expected Output:

 GET https://clapptest.cimconlms.com/SLCScannerV2/api-bct/setting/261

{
  "status": "success",
  "data": {
    "client_slc_list_view": "Yes",
    "client_slc_edit_view": "Yes",
    "client_slc_pole_image_view": "Yes",
    "client_slc_pole_assets_view": "Yes",
    "client_slc_pole_id": "Yes"
  }
}

GET:  https://clapptest.cimconlms.com/SLCScannerV2/api-bct/change-language/2156/es/261
{
  "status": "success",
  "msg": "success",
  "language": "es",
  "ScanLBL": "UID",
  "ScanPH": "UID"
}
*/