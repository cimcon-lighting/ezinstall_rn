
import { API_SLC_LIST, POST } from '../utility/config'
import { networkManager } from '../action/NetworkManager'

import { API_PENDING, API_SUCCESS, API_ERROR } from '../action/action.types';
import { SLC_LIST_PENDING, SLC_LIST_SUCCESS, SLC_LIST_ERROR } from '../action/action.types';
import { getLocation, getClientDetails } from '../utility/Utility'


export const getSLCList = (data1) => async (dispatch) => {

    console.log("AAYAVU")
    const { latitude, longitude } = await getLocation()    
    const { pageNo, fromDate, toDate, slcLowerCase, slcID } = data1
    const { userid } = await getClientDetails()
    const lat = latitude
    const long = longitude

    console.log("SLCLIST API ========= " + lat + "-----------" + long + "-----------" + 
                    fromDate + "-----------" + toDate + "-----------" + slcLowerCase + "-----------" + slcID);

    const requestObject = {
        search : slcID,
        node_type: "",
        from_daterange: fromDate,
        to_daterange: toDate,
        slc_type: slcLowerCase,
    }

    const request = {
        url: `${API_SLC_LIST}/${userid}/${pageNo}/${lat}/${long}`,
        method: POST,
        data: requestObject
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        const data = {
            type: SLC_LIST_SUCCESS,
            payload: response
        }
        dispatch(data)
    } else if (response.type == API_ERROR) {
        const data = {
            type: SLC_LIST_ERROR,
            payload: response
        }
        dispatch(data)
    }
}
