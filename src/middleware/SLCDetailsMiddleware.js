
import { API_SLC_DETAILS, GET } from '../utility/config'
import { networkManager } from '../action/NetworkManager'

import { API_SUCCESS, API_ERROR } from '../action/action.types';
import { SLC_DETAILS_SUCCESS, SLC_DETAILS_ERROR } from '../action/action.types';


export const getSLCDetails = ({ ID }) => async (dispatch) => {

    console.log("SLC_DETAILS API ========= " + "-----------" + ID);

    const request = {
        url: `${API_SLC_DETAILS}/${ID}`,
        method: GET,
        data: {}
    }

    let response = await networkManager(request)

    if (response.type == API_SUCCESS) {
        const data = {
            type: SLC_DETAILS_SUCCESS,
            payload: response
        }
        dispatch(data)
    } else if (response.type == API_ERROR) {
        const data = {
            type: SLC_DETAILS_ERROR,
            payload: response
        }
        dispatch(data)
    }
}
