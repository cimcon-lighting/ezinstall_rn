import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {NativeBaseProvider, Spinner,Box} from 'native-base';

const EmptyContainer = () => {
  return (
    <NativeBaseProvider>
      <Box style={styles.emptyContainer}>
        <Spinner />
      </Box>
    </NativeBaseProvider>
  );
};

export default EmptyContainer;

const styles = StyleSheet.create({
  emptyContainer: {
    flex: 1,
    backgroundColor: '#0072BC',
    justifyContent: 'center',
    alignContent: 'center',
  },
});