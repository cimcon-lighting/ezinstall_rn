
import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';

import { Modal } from 'native-base'

const ProgressDialog = ({ visible }) => (


    <Modal onTouchCancel={false} isOpen={visible}>
        <View style={{ backgroundColor: "white", padding: 20, borderRadius: 10, alignItems: "center" }}>
            {/* <Text style={{ fontSize: 20 }}>{message}</Text> */}
            <ActivityIndicator size="large" />
        </View>
    </Modal>

    // <Modal onRequestClose={() => null} visible={visible}>
    //     <View style={{ flex: 1, backgroundColor: "#dcdcdc", justifyContent: "center", alignItems: "center" }}>
    //         <View style={{ backgroundColor: "white", padding: 20, borderRadius: 10, alignItems: "center" }}>
    //             {/* <Text style={{ fontSize: 20 }}>{message}</Text> */}
    //             <ActivityIndicator size="large" />
    //         </View>
    //     </View>
    // </Modal>
);


export default ProgressDialog;