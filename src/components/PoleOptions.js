import React, { useState, useEffect }from 'react'
import { 
    TouchableOpacity, 
    View } from 'react-native'
import {
    NativeBaseProvider, 
    Text,
    Box, 
    HStack,
    Checkbox,
    FormControl,
    Input
    } from 'native-base'

import { StyleSheet, TouchableWithoutFeedback } from 'react-native'

import Icon from 'react-native-vector-icons/dist/Ionicons'
import IconInsta from 'react-native-vector-icons/dist/AntDesign'

import { SLC_DETAILS_TITLE } from '../utility/Constant'

const PoleOptions = ({ item, navigation }) => {


    // OPTIONS: { "PeopleCounter": "0", "PeopleCounter_text": "", "key": "PeopleCounter", "value": "People Counter" }
    // (item.key)_text
    // LOG  FINSL :  PeopleCounter_text

    console.log("OPTIONS : ",item)

    let textValue = item.key + "_text"
    console.log("FINSL : ", textValue)
    console.log("YO YO YO :", item[textValue])

    return (
        <NativeBaseProvider>
             <TouchableWithoutFeedback onPress={ () => {} }>
                <Box style = {styles.box}>
                    <HStack >
                        <View style = {{ width:"45%", paddingTop : 3}}>
                        <Checkbox
                            colorScheme="green"
                            onPress={() => {
                                // setChecked(!checked)
                            }}>
                                <Text style={styles.headingLable}> {item.value} </Text>
                            </Checkbox>
                        </View>
                        <FormControl isRequired
                            style={{ paddingLeft: 40 }}>
                            <Input
                                backgroundColor="white"
                                w = {"45%"}
                                 placeholder="Search by SLC ID"
                                value={item[textValue] || " "}
                                 fontSize={16}
                                        onChangeText={(value) => {
                                        
                                    }}
                                 color="#474a4d" />
                         </FormControl>
                    </HStack>
                </Box>
                </TouchableWithoutFeedback>
            {/* <View style = {{backgroundColor : "#01317B", height : 0.4}}></View> */}
        </NativeBaseProvider> 
    )
}

export default PoleOptions;

const styles = StyleSheet.create( {
    box : {
        flex : 1,
        margin: 8,
        paddingLeft : 12,
        justifyContent : 'center'
    },
    headingLable : {
        color : "#FFF",
        fontSize: 17,
        padding : 5
    },
    collanLabel : {
        color : "#FFF",
        // fontWeight : "bold",
        margin : 3,
        fontSize : 17,
        width : "3%"
    },
    lable : {
        color : "#FFF",
        // fontWeight : 'normal',
        margin : 2,
        fontSize : 17,
        flex : 1
    },
   
})