import React from 'react'
import { 
    View } from 'react-native'
import {
    NativeBaseProvider, 
    Text,
    Box, 
    HStack
    } from 'native-base'

import { StyleSheet } from 'react-native'

const SLCDetailsRaw = ({ item }) => {
    // console.log('ASSESTES Item :',item.AttributeName);

    return (
        <NativeBaseProvider>
            <Box style = {styles.box}>
                <HStack >
                    <Text style = {styles.headingLable}> {item.AttributeName}</Text>
                    {/* <Text style = {styles.collanLabel}>:</Text>  */}
                    <Text style = {styles.lable}>{item.Selected}</Text>
                </HStack>
            </Box>
        </NativeBaseProvider> 
    )

   
}

export default SLCDetailsRaw;

const styles = StyleSheet.create( {
    box : {
        flex : 1,
        margin : 8,
        justifyContent : 'space-around'
    },
    headingLable : {
        color : "#222222",
        // fontWeight : "bold",
        margin : 3,
        fontSize : 15,
        width : "50%"
    },
    collanLabel : {
        color : "#FFF",
        fontWeight : "bold",
        margin : 3,
        fontSize : 16,
        width : "3%"
    },
    lable : {
        color : "#585858",
        fontWeight : 'normal',
        margin : 2,
        fontSize : 15,
        flex: 1,
        textAlign: 'center',
    },
   
})