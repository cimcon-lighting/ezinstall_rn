import React, { useState, useEffect } from 'react'

import { connect, useDispatch } from 'react-redux'
import propTypes from 'prop-types'
import { Button, Modal, Input, FormControl, } from 'native-base'
import { doCheckValidSLCId } from '../middleware/AddSLCIdMiddleware'
import { Alert } from 'react-native';

const AddSLCDialog = ({ slcValidStatus, slcValue, doCheckValidSLCId, macValue, isDialogShow, onPress, showProgress, closeProgress }) => {

    console.log(isDialogShow);
    const [showDialog, setShowDialog] = useState(false)
    const [inputValue, setInputValue] = useState("")
    const [isValidId, setIsValidId] = useState(false)
    const [errorMsg, setErrorMsg] = useState('')

    useEffect(() => {
        setShowDialog(isDialogShow)
        setInputValue(slcValue)
    }, [isDialogShow])


    if (slcValidStatus.error != '') {
        setErrorMsg(slcValidStatus.error)
        setIsValidId(true)
        if (showDialog == false) {
            setShowDialog(true)
        }
        slcValidStatus.error = ''
        closeProgress()
    }

    if (slcValidStatus.data != '') {
        closeProgress()
        onPress()
        console.log("daata >>>>>>>" + slcValidStatus.data);
        ContinueDialogOpen({ macValue: inputValue, apiResponse: slcValidStatus.data, showProgress })
        slcValidStatus.data = ''
    }

    return (
        <Modal isOpen={showDialog} onClose={() => setShowDialog(false)}>
            <Modal.Content w="80%">

                <Modal.Header>Enter SLC ID</Modal.Header>
                <Modal.Body>
                    <FormControl
                        isInvalid={
                            isValidId
                        }
                    >
                        <Input
                            returnKeyType={"next"}
                            value={inputValue}
                            onChangeText={(text) => {
                                setIsValidId(false)
                                setInputValue(text)
                            }}
                            variant="outline" placeholder='Please enter value' />
                        <FormControl.ErrorMessage>
                            {errorMsg}
                        </FormControl.ErrorMessage>
                    </FormControl>
                </Modal.Body>
                <Modal.Footer>
                    <Button.Group space={2}>
                        <Button
                            variant="ghost"
                            colorScheme="blueGray"
                            onPress={() => {
                                setIsValidId(false)
                                onPress()
                            }}
                        >
                            Cancel
                        </Button>
                        <Button
                            onPress={() => {

                                if (inputValue == '') {
                                    setErrorMsg('Please enter or scan value')
                                    setIsValidId(true)
                                } else {
                                    console.log("Show progress bar call");
                                    showProgress()
                                    doCheckValidSLCId({ slcid: inputValue, macAddress: macValue })
                                }
                            }}
                        >
                            Next
                        </Button>
                    </Button.Group>
                </Modal.Footer>
            </Modal.Content>
        </Modal>
    )
}

AddSLCDialog.prototype = {
    doCheckValidSLCId: propTypes.func.isRequired,
    slcValidStatus: propTypes.object.isRequired,
}

const mapDispatchToProps = {
    doCheckValidSLCId: (data) => doCheckValidSLCId(data),
}

const mapStateToProps = state => ({
    slcValidStatus: state.AddSLCReducers,
})

export default connect(mapStateToProps, mapDispatchToProps)(AddSLCDialog);

export const ContinueDialogOpen = (data) => {

    const dispatch = useDispatch();
    const { macValue, apiResponse, showProgress } = data

    return (
        Alert.alert(
            "Alert!!!",
            apiResponse.mac_address_type,
            [
                {
                    text: "No",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "Yes", onPress: () => {
                        console.log("OK Pressed")
                        showProgress()
                        validateMacAddressType({ value: macValue, response: apiResponse, dispatch })
                    }
                }
            ]
        )
    )
}