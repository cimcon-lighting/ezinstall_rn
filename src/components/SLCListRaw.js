import React, { useState, useEffect }from 'react'
import { 
    TouchableOpacity, 
    View } from 'react-native'
import {
    NativeBaseProvider, 
    Text,
    Box, 
    HStack
    } from 'native-base'

import { StyleSheet, TouchableWithoutFeedback } from 'react-native'

import Icon from 'react-native-vector-icons/dist/Ionicons'
import IconInsta from 'react-native-vector-icons/dist/AntDesign'

import { SLC_DETAILS_TITLE } from '../utility/Constant'

const SLCListRaw = ({ item, navigation }) => {
    // console.log("SLC LIST : " + item.address)

    const actionOnRow = (item) => {
        // console.log('Selected Item :',item.ID);
        navigation.navigate(SLC_DETAILS_TITLE, {
            item: item,
            otherParam: 'anything you want here',
        })
    }

    return (
        <NativeBaseProvider>
             <TouchableWithoutFeedback onPress={ () => actionOnRow(item) }>
                <Box style = {styles.box}>
                    <HStack >
                        <Text style = {styles.headingLable}> SLC ID</Text>
                        <Text style = {styles.collanLabel}>:</Text> 
                        <Text style = {styles.lable}>{item.slc_id}</Text>
                    </HStack>
                    <HStack >
                        <Text style = {styles.headingLable}> POLE ID </Text> 
                        <Text style = {styles.collanLabel}>:</Text> 
                        <Text style = {styles.lable}>{item.pole_id}</Text>
                    </HStack>
                    <HStack >
                        <Text style = {styles.headingLable}> ADDED ON</Text> 
                        <Text style = {styles.collanLabel}>:</Text> 
                        <Text style = {styles.lable}>{item.created}</Text>
                    </HStack>
                    <HStack >
                        <Text style = {styles.headingLable}> ADDRESS </Text> 
                        <Text style = {styles.collanLabel}>:</Text> 
                        <Text style = {styles.lable}>{item.address}</Text>
                    </HStack>
                </Box>
                </TouchableWithoutFeedback>
            <View style = {{backgroundColor : "#01317B", height : 0.4}}></View>
        </NativeBaseProvider> 
    )
}

export default SLCListRaw;

const styles = StyleSheet.create( {
    box : {
        flex : 1,
        margin : 8,
        justifyContent : 'space-around'
    },
    headingLable : {
        color : "#FFF",
        fontWeight : "bold",
        margin : 3,
        fontSize : 16,
        width : "25%"
    },
    collanLabel : {
        color : "#FFF",
        fontWeight : "bold",
        margin : 3,
        fontSize : 16,
        width : "3%"
    },
    lable : {
        color : "#FFF",
        fontWeight : 'normal',
        margin : 2,
        fontSize : 16,
        flex : 1
    },
   
})