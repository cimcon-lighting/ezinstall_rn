import React, { useState, useEffect } from 'react'

import { connect, useDispatch } from 'react-redux'
import propTypes from 'prop-types'
import { Button, Modal, NativeBaseProvider, Input, FormControl, } from 'native-base'
import { doCheckValidMacAddress, validateMacAddressType } from '../middleware/AddMacIdMiddleware'
import { Alert } from 'react-native';

const AddMacDialog = ({ macValidStatus, doCheckValidMacAddress, subTitle, macValue, isDialogShow, onPress, showProgress, closeProgress }) => {

    console.log(isDialogShow);
    const [showDialog, setShowDialog] = useState(false)
    const [inputValue, setInputValue] = useState("")
    const [isValidId, setIsValidId] = useState(false)
    const [errorMsg, setErrorMsg] = useState('')

    useEffect(() => {
        setShowDialog(isDialogShow)
        setInputValue(macValue)
    }, [isDialogShow])


    if (macValidStatus.error != '') {
        setErrorMsg(macValidStatus.error)
        setIsValidId(true)
        if (showDialog == false) {
            setShowDialog(true)
        }
        macValidStatus.error = ''
        closeProgress()
    }

    if (macValidStatus.data != '') {
        closeProgress()
        onPress()
        console.log("daata >>>>>>>" + macValidStatus.data.mac_address_type);
        ContinueDialogOpen({ macValue: inputValue, apiResponse: macValidStatus.data, showProgress })
        macValidStatus.data = ''
    }

    return (
        <Modal isOpen={showDialog} onClose={() => setShowDialog(false)}>
            <Modal.Content w="80%">

                <Modal.Header>{subTitle}</Modal.Header>
                <Modal.Body>
                    <FormControl
                        isInvalid={
                            isValidId
                        }
                    >
                        <Input
                            returnKeyType={"next"}
                            value={inputValue}
                            onChangeText={(text) => {
                                setIsValidId(false)
                                setInputValue(text)
                            }}
                            variant="outline" placeholder='Please enter value' />
                        <FormControl.ErrorMessage>
                            {errorMsg}
                        </FormControl.ErrorMessage>
                    </FormControl>
                </Modal.Body>
                <Modal.Footer>
                    <Button.Group space={2}>
                        <Button
                            variant="ghost"
                            colorScheme="blueGray"
                            onPress={() => {
                                setIsValidId(false)
                                onPress()
                            }}
                        >
                            Cancel
                        </Button>
                        <Button
                            onPress={() => {

                                if (inputValue == '') {
                                    setErrorMsg('Please enter or scan value')
                                    setIsValidId(true)
                                } else {
                                    console.log("Show progress bar call");
                                    showProgress()
                                    doCheckValidMacAddress({ macValue: inputValue })
                                }
                            }}
                        >
                            Next
                        </Button>
                    </Button.Group>
                </Modal.Footer>
            </Modal.Content>
        </Modal>
    )
}

AddMacDialog.prototype = {
    doCheckValidMacAddress: propTypes.func.isRequired,
    macValidStatus: propTypes.object.isRequired,
}

const mapDispatchToProps = {
    doCheckValidMacAddress: (data) => doCheckValidMacAddress(data),
}

const mapStateToProps = state => ({
    macValidStatus: state.AddMacReducers,
})

export default connect(mapStateToProps, mapDispatchToProps)(AddMacDialog);

export const ContinueDialogOpen = (data) => {

    const dispatch = useDispatch();
    const { macValue, apiResponse, showProgress } = data

    return (
        Alert.alert(
            "Alert!!!",
            apiResponse.mac_address_type,
            [
                {
                    text: "No",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                {
                    text: "Yes", onPress: () => {
                        console.log("OK Pressed")
                        showProgress()
                        validateMacAddressType({ value: macValue, response: apiResponse, dispatch })
                    }
                }
            ]
        )
    )
}