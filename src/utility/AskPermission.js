import {PermissionsAndroid, ToastAndroid} from 'react-native';

export const requestPermission = async () => {
  try {
    const granted = await PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE,
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  
    ]);
    console.log(granted);

    if (
      granted['PermissionsAndroid.PERMISSIONS.READ_PHONE_STATE'] ===
        'denied') {
      ToastAndroid.show(
        'We can not process without permission',
        ToastAndroid.LONG,
      );
      requestPermission();
    }
  } catch (error) {
    console.error(error);
  }
};
