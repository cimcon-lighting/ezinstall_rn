
export const BASE_URL = 'https://clapptest.cimconlms.com/'
export const SERVER_CONTEXT_PATH = 'SLCScannerV2/api-bct/'
export const POST = 'POST'
export const GET = 'GET'


//API LIST
export const API_LOGIN = 'userLogin'
export const API_SLC_LIST       = 'slclist'
export const API_SECURITY_CODE = 'checkuniquecode'
export const API_SLC_DETAILS = 'slc'
export const API_CHECK_INTERNAL_UNIQUE_MACADDRESS = 'checkInternalUniqueMacAddressAPI'
export const API_CHECK_MAC_IN_LG = 'checkmacaddress'
export const API_SLC_LIST_USING_LATLONG='getSlcListUsingLatLong'
export const API_CHECK_SLCID = 'checkslcid'
export const API_CHECK_MAC_WITH_SLC = 'checkMacAddressSlcIdbeforeSave'
export const API_SETTING ='setting'
export const API_CHANGE_LANGUAGE='change-language'
