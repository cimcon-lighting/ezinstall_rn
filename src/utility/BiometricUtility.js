
import ReactNativeBiometrics from 'react-native-biometrics'
import { useDispatch } from 'react-redux'

import { BIOMETRIC_SUPPORTED, BIOMETRIC_SUCCESS, BIOMETRIC_FAIED } from '../action/action.types'



export const isBiometricSupported = () => (dispatch) => {

    console.log("Checking biometric supported or not.");

    ReactNativeBiometrics.isSensorAvailable()
        .then((resultObject) => {

            const { available, biometryType, error } = resultObject
            console.log(`Available - > ${available}, biometryType -> ${biometryType}, error -> ${error}`);

            if (available && biometryType === ReactNativeBiometrics.TouchID) {
                console.log('TouchID is supported')
                setBioDispatch(dispatch, BIOMETRIC_SUPPORTED, 'true')
            } else if (available && biometryType === ReactNativeBiometrics.FaceID) {
                console.log('FaceID is supported')
                setBioDispatch(dispatch, BIOMETRIC_SUPPORTED, 'true')
            } else if (available && biometryType === ReactNativeBiometrics.Biometrics) {
                console.log('Biometrics is supported')
                setBioDispatch(dispatch, BIOMETRIC_SUPPORTED, 'true')
            } else {
                console.log('Biometrics not supported')
                setBioDispatch(dispatch, BIOMETRIC_SUPPORTED, 'false')
            }
        })
        .catch((error) => {
            console.log('Biometrics not supported, Error is ' + error)
        })
}

function setBioDispatch(dispatch, type, payload) {

    const data = {
        type: type,
        payload: payload
    }
    dispatch(data)
}


export const invokeBiometric = () => (dispatch) => {

    ReactNativeBiometrics.simplePrompt({ promptMessage: 'Confirm fingerprint' })
        .then((resultObject) => {
            const { success } = resultObject

            if (success) {
                console.log('successful biometrics provided')
                setBioDispatch(dispatch, BIOMETRIC_SUCCESS, BIOMETRIC_SUCCESS)
            } else {
                console.log('user cancelled biometric prompt')
                setBioDispatch(dispatch, BIOMETRIC_FAIED, BIOMETRIC_FAIED)
            }
        })
        .catch(() => {
            console.log('biometrics failed')
            setBioDispatch(dispatch, BIOMETRIC_FAIED, BIOMETRIC_FAIED)
        })
}