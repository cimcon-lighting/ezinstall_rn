import Geolocation from 'react-native-geolocation-service';
import { setLocation } from '../utility/Utility'

import {
    PermissionsAndroid,
    Alert,
    Linking,
    Platform,
    ToastAndroid
} from 'react-native'


export const hasPermissionIOS = async () => {

    console.log("hasPermissionIOS");

    const openSetting = () => {
        Linking.openSettings().catch(() => {
            Alert.alert('Unable to open settings');
        });
    };
    const status = await Geolocation.requestAuthorization('whenInUse');

    if (status === 'granted') {
        return true;
    }

    if (status === 'denied') {
        Alert.alert('Location permission denied');
    }

    if (status === 'disabled') {
        Alert.alert(
            `Turn on Location Services to allow EZInstall to determine your location.`,
            '',
            [
                { text: 'Go to Settings', onPress: openSetting },
                { text: "Don't Use Location", onPress: () => { } },
            ],
        );
    }

    return false;
}

export const hasLocationPermission = async () => {

    console.log("hasLocationPermission");

    if (Platform.OS === 'ios') {
        const hasPermission = await hasPermissionIOS();
        return hasPermission;
    }

    if (Platform.OS === 'android' && Platform.Version < 23) {
        return true;
    }

    const hasPermission = await PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) {
        return true;
    }

    const status = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
        return true;
    }

    if (status === PermissionsAndroid.RESULTS.DENIED) {
        ToastAndroid.show(
            'Location permission denied by user.',
            ToastAndroid.LONG,
        );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
        ToastAndroid.show(
            'Location permission revoked by user.',
            ToastAndroid.LONG,
        );
    }

    return false;
}

export const getLocation = async () => {

    console.log("getting user current location.");
    const hasPermission = await hasLocationPermission();
    console.log("Check permission : " + hasPermission);

    if (!hasPermission) {
        return;
    }

    Geolocation.getCurrentPosition(
        (position) => {
            console.log("My current location", JSON.stringify(position));
            const geoCoordinates = position.coords;
            console.log("latitude : " + position.coords.latitude);
            console.log("longitude : " + position.coords.longitude);
            setLocation(geoCoordinates);
        },
        (error) => {
            Alert.alert(`Code ${error.code}`, error.message);
            console.log(error);
        },
        {
            accuracy: {
                android: 'high',
                ios: 'best',
            },
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 10000,
            showLocationDialog: true,
        },
    );
}
