import AsyncStorage from "@react-native-async-storage/async-storage";
import { LOCATION, IS_SECURITY_CODE_EXIST, CLIENT_DETAILS,SETTING_DETAILS } from "./Constant";

import React from 'react';
import { getUniqueId, getVersion } from 'react-native-device-info';
import {
  TouchableWithoutFeedback,
  Keyboard,
  View,
  NativeModules,
  StatusBar
} from 'react-native';

const DismissKeyboardHOC = (Comp) => {
  return ({ children, ...props }) => (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <Comp {...props}>
        {children}
      </Comp>
    </TouchableWithoutFeedback>
  );
};

export const DismissKeyboardView = DismissKeyboardHOC(View)

export const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 0 : (StatusBar.currentHeight);

export const setLocation = async (location) => {
  console.log("Location store in local");
  await AsyncStorage.setItem(LOCATION, JSON.stringify(location));
};

export const getLocation = async () => {
  console.log("getting the location from the local");
  const location = await AsyncStorage.getItem(LOCATION)
  const locationParse = await JSON.parse(location)
  return locationParse;
};

export const setSecurtiyCodeExist = async (value) => {
  try {
    await AsyncStorage.setItem(IS_SECURITY_CODE_EXIST, value);
  } catch (e) {
    // saving error
    console.log(e);
  }
};

export const isSecurtiyCodeExist = async () => {
  try {
    const value = await AsyncStorage.getItem(IS_SECURITY_CODE_EXIST);
    if (value !== null) {
      // value previously stored
      console.log(value);
      return value;
    }

  } catch (e) {
    // error reading value
    console.log(e)
  }
};

export const clearAllData = () => {
  AsyncStorage.getAllKeys()
    .then(keys => AsyncStorage.multiRemove(keys))
    .then(() => {
      console.log("succefully clear storage !")
      //alert("succefully Logout! !");
    
    });
}

export const getPlatform = () => {

  if (Platform.OS === 'ios') {
    console.log('IOS platform');
    return 'IOS'
  } else {
    console.log('Android platform');
    return 'Android'
  }
}


export const setClientDetails = async (secuirty_code_response) => {
  try {
    console.log("Securtiy code response in local");
    await AsyncStorage.setItem(
      CLIENT_DETAILS,
      JSON.stringify(secuirty_code_response)
    );
  } catch (e) {
    console.log(e);
  }
};

export const getClientDetails = async () => {
  const jsonValue = await AsyncStorage.getItem(CLIENT_DETAILS);
  const locationParse = await JSON.parse(jsonValue)

  return jsonValue != null ? locationParse : null;
};

export const setisLoggedIn = async (value) => {
  try {
    await AsyncStorage.setItem(IS_LOGGED_IN, JSON.stringify(value));
  } catch (e) {
    // saving error
    console.log(e);
  }
};

export const isLoggedIn = async () => {
  try {
    const value = JSON.parse(await AsyncStorage.getItem(IS_LOGGED_IN));
    if (value !== null) {
      // value previously stored
      console.log("isLoggedIn:" + value);
      return value;
    }
  } catch (e) {
    // error reading value
    console.log(e);
    return false;
  }
};

export const getStorageKey = async (key) => {
  const value = await AsyncStorage.getItem(key)
  console.log(`GET VALUE - Key : ${key}, Value: ${value} `);
  return value != null ? value : ''
};

export const setStorageKey = async (key, value) => {
  try {
    console.log(`Store VALUE - Key : ${key}, Value: ${value} `);
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    console.log(e);
  }
};

export const getLanguage = () => {

  const language =

    Platform.OS === "ios"

      ? NativeModules.SettingsManager.settings.AppleLocale ||

      NativeModules.SettingsManager.settings.AppleLanguages[0] //iOS 13

      : NativeModules.I18nManager.localeIdentifier;

  console.log(language);

  return language;

};

export const getDeviceUniqueId = () => {
  const uniqueId = getUniqueId()
  console.log("Unique id: " + uniqueId)
  return uniqueId;
}

export const getAppVersion = () => {
  const appVersion = getVersion()
  console.log("App Version " + appVersion)
  return appVersion;

}

export const setSetting = async (secuirty_code_response) => {
  try {
    console.log("Setting data stores in local");
    await AsyncStorage.setItem(
      SETTING_DETAILS,
      JSON.stringify(secuirty_code_response)
    );
  } catch (e) {
    console.log(e);
  }
};

export const getSetting = async () => {
  const jsonValue = await AsyncStorage.getItem(SETTING_DETAILS);
  const locationParse = await JSON.parse(jsonValue)

  return jsonValue != null ? locationParse : null;
};