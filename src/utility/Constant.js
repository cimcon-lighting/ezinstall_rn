export const LOCATION = "location";
export const IS_LOGGED_IN = "is_logged_in";
export const IS_SECURITY_CODE_EXIST = "is_security_code_exist";
export const CLIENT_DETAILS = "client_details";
export const SETTING_DETAILS="setting_details";

//STOARGE KEYS
export const KEY_SECURITY_CODE = "securityCode";
export const KEY_USERNAME = "username";
export const KEY_PASSWORD = "password";
export const KEY_IS_REMEMBER = "isRemember";
export const KEY_CUSTOMER_NAME = "customerName";
export const KEY_LOGIN_TOKEN = "loginToken";
export const KEY_MAP_TYPE="map_type"
export const KEY_UNIQUE_DEVICE_ID="unique_device_id"
export const KEY_LANGUAGE_TYPE="language_type"
export const KEY_MEASURMENT_UNIT="measurment_unit"
export const KEY_IS_APPLE_MAP_TYPE_SELECTED="key_is_apple_map_type_selected"

//SCREEN TITLE
export const SLC_DETAILS_TITLE = "POLE DETAILS";
export const MAP_TITLE="MAP";
export const SLC_LIST_TITLE = "LIST"
export const MAC_SCAN_TITLE = "SCAN"
export const NOTES_TITLE = "NOTES"
export const SLC_SCAN_TITLE = "SCAN SLC ID"
export const SETTING_TITLE="SETTINGS";

//Map Type:
export const MAP_STANDARD="standard"
export const MAP_HYBRID="hybrid"
export const MAP_SATELLITE="satellite"
export const MAP_OPEN_STREET="openstreet"

export const MAP_STANDARD_IOS="standard_ios"
export const MAP_HYBRID_IOS="hybrid_ios"
export const MAP_SATELLITE_IOS="satellite_ios"

//Language code:
export const LANGUAGE_CODE_ENGLISH="en"
export const LANGUAGE_CODE_SPANISH="es"
export const LANGUAGE_CODE_PORTUGUES="pt"

//Measurment Unit
export const UNIT_ENGLISH="English"
export const UNIT_METIRC="Metric"
